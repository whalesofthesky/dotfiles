#! /usr/bin/env sh

nvidia-settings --assign CurrentMetaMode="nvidia-auto-select +0+0 { ForceFullCompositionPipeline = On }"
xrandr --addmode DP-4 2560x1440
xrandr --output DP-4 --primary --pos 0x0 --mode 2560x1440
xrandr --output HDMI-0 --off
xrandr --output HDMI-0 --right-of DP-4 --mode 1920x1080 --auto
#feh --bg-scale $IAN_BACKGROUND_PATH
