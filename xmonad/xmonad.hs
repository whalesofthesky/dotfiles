import XMonad
import XMonad.Actions.CycleWS
import XMonad.Hooks.DynamicLog
-- https://bbs.archlinux.org/viewtopic.php?id=184406
import XMonad.Hooks.EwmhDesktops(fullscreenEventHook,ewmh)
import XMonad.Hooks.InsertPosition
import XMonad.Hooks.ManageDocks
import XMonad.Layout.LayoutCombinators
import XMonad.Layout.Spacing
import XMonad.Util.Cursor
import XMonad.Util.SpawnOnce
import XMonad.Util.EZConfig(additionalKeys)
import XMonad.Util.Run(spawnPipe)
import qualified XMonad.StackSet as W
import System.IO

myStartupHook = composeAll
  [spawnOnce "picom"
  , spawnOnce "dunst"
  , spawnOnce "feh --bg-scale $IAN_BACKGROUND_PATH"
  , spawnOnce "emacs"
  , setDefaultCursor xC_left_ptr
  ]
-- myStartupHook = composeAll []

myManageHook = composeAll
  [ className =? "zoom" --> doFloat
  , className =? "Steam" --> doFloat
  -- TODO: If xmonad can preserve the aspect ratio that these windows
  --       want to open with, then this will work with tiling
  --       maybe layout hints are relevant? 
  --
  -- http://hackage.haskell.org/package/xmonad-contrib-0.16/docs/XMonad-Layout-LayoutHints.html
  , className =? "R_x11" --> doFloat
  -- TODO: Maybe only do this on certain layouts?
  -- https://www.reddit.com/r/xmonad/comments/9vjml0/how_can_i_make_new_windows_not_go_to_first_spot/
  , insertPosition Below Newer
  ]

-- myLayoutHook = composeAll [smartBorders $
--                           ]
-- Command to launch the bar.
myBar = "/usr/bin/xmobar"

-- Custom PP, configure it as you like. It determines what is being written to the bar.
myPP = xmobarPP

-- Key binding to toggle the gap for the bar.
toggleStrutsKey XConfig {XMonad.modMask = modMask} = (modMask, xK_b)

myConfig = def { modMask = mod4Mask }

rofiDrun = do
  curscreen <-
      (fromIntegral . W.screen . W.current) `fmap` gets windowset :: X Int
  spawn $ "rofi -show drun -m " ++ show (1 - curscreen)

rofiWindow = do
  curscreen <-
      (fromIntegral . W.screen . W.current) `fmap` gets windowset :: X Int
  spawn $ "rofi -show window -m " ++ show (1 - curscreen)

-- mySetSpacing a = a
mySetSpacing = spacingRaw True (Border 5 5 5 5) True (Border 5 5 5 5) True

gotoEmpty = moveTo Next EmptyWS

main = do
  statusBar myBar myPP toggleStrutsKey myConfig
  xmonad $ ewmh $ docks def
    {modMask = mod4Mask
    , layoutHook = avoidStruts $ mySetSpacing $ layoutHook def -- <+> myLayoutHook
    , startupHook = myStartupHook
    , manageHook = myManageHook
    , handleEventHook = handleEventHook def <+> docksEventHook
    , terminal = "alacritty"
    }
    `additionalKeys`
    [ ((mod4Mask, xK_p), rofiDrun)
    , ((mod4Mask, xK_o), rofiWindow)
    , ((mod4Mask, xK_Tab), nextWS)
    , ((mod4Mask .|. shiftMask, xK_Tab), prevWS)
    , ((mod4Mask, xK_n), gotoEmpty)
    -- ,((mod4Mask, xK_b     ), void (sendXmobar "Toggle 0") <+> broadcastMessage ToggleStruts <+> refresh)
    ,((mod4Mask, xK_b), broadcastMessage ToggleStruts <+> refresh)
    -- see /usr/include/X11/XF86keysym.h
    -- depends on alsa-utils
    , ((0, 0x1008FF11), spawn "amixer -q sset Master 2%-")
    , ((0, 0x1008FF13), spawn "amixer -q sset Master 2%+")
    , ((0, 0x1008FF12), spawn "amixer set Master toggle")
    -- , ((mod4Mask, xK_f), sendMessage $ JumpToLayout "FullScreen")
    ]
