" Fold Settings {
"  vim: set foldmarker={,} foldlevel=0 foldmethod=marker spell:
" }

" Environment {
    " Basics {
        " remove for neovim!
        set nocompatible        " must be first line
        " neovim should work with just unnamedplus (plz confirm!)
        if has ("unix") && "Darwin" != system("echo -n \"$(uname)\"")
            " on Linux use + register for copy-paste
            set clipboard=unnamedplus
        else
            " one mac and windows, use * register for copy-paste
            set clipboard=unnamed
        endif
    " }

    " Windows Compatible {
        " On Windows, also use '.vim' instead of 'vimfiles'; this makes synchronization
        " across (heterogeneous) systems easier.
        if has('win32') || has('win64')
          set runtimepath=$HOME/.vim,$VIM/vimfiles,$VIMRUNTIME,$VIM/vimfiles/after,$HOME/.vim/after
        endif
    " }

    " Filetype Workarounds {
        " Temporary workaround to Better-CSS-Syntax-for-Vim
        " See https://github.com/ChrisYip/Better-CSS-Syntax-for-Vim/issues/9
        " for more information
        autocmd BufNewFile,BufRead *.sass set filetype=scss
        autocmd BufNewFile,BufRead *.less set filetype=css
    " }

    " Detect OS {
        if !exists("g:os")
            if has("win64") || has("win32") || has("win16")
                let g:os = "Windows"
            else
                let g:os = substitute(system('uname'), '\n', '', '')
            endif
        endif
    " }
" }

" General {
    " already default on nvim
    set background=dark         " Assume a dark background
    color desert
    if !has('gui')
        "set term=$TERM          " Make arrow and other keys work
    endif
    " already default on nvim
    filetype plugin indent on   " Automatically detect file types.
    " already default on nvim
    syntax on                   " syntax highlighting
    set mouse=a                 " automatically enable mouse usage
    " may not need this at all? 
    scriptencoding utf-8

    " set autowrite                 " automatically write a file when leaving a modified buffer
    set shortmess+=filmnrxoOtT      " abbrev. of messages (avoids 'hit enter')
    " nvim - unix and slash are deprecated, the rest are already enabled by default
    set viewoptions=folds,options,cursor,unix,slash " better unix / windows compatibility
    set virtualedit=onemore         " allow for cursor beyond last character
    " nvim - default is 10,000 so we don't need this
    set history=1000                " Store a ton of history (default is 20)
    " nvim - nospell is already default
    set nospell                     " spell checking off
    set hidden                      " allow buffer switching without saving
    set completeopt=menu            " prevent autodoc for python

    " Backup and Persistent Undo {
        set backup

        if has('persistent_undo')
            set undofile                "so is persistent undo ...
            set undolevels=1000         "maximum number of changes that can be undone
            set undoreload=10000        "maximum number lines to save for undo on a buffer reload
        endif

        function! EnsureDirectoryExists(directory)
            if exists("*mkdir")
                if !isdirectory(a:directory)
                    call mkdir(a:directory)
                endif
            endif
        endfunction
        function! InitializeDirectories()
            let separator = "."
            let parent = $HOME
            let prefix = '.vim'
            let dir_list = {
                        \ 'backup': 'backupdir',
                        \ 'views': 'viewdir',
                        \ 'swap': 'directory' }

            if has('persistent_undo')
                let dir_list['undo'] = 'undodir'
            endif

            let vim_directory = parent . '/' . prefix . '/'
            call EnsureDirectoryExists(vim_directory)
            for [dirname, settingname] in items(dir_list)
                let directory = parent . '/' . prefix . '/' . dirname . '/'
                call EnsureDirectoryExists(directory)
                if !isdirectory(directory)
                    echo "Warning: Unable to create backup directory: " . directory
                    echo "Try: mkdir -p " . directory
                else
                    let directory = substitute(directory, " ", "\\\\ ", "g")
                    exec "set " . settingname . "=" . directory
                endif
            endfor
        endfunction
        call InitializeDirectories()
    " }

    if has("gui_running")
        set guioptions=egrt
    endif

    " Searching
    " nvim - already default
    set hlsearch
    " nvim - already default
    set incsearch
    set ignorecase
    set smartcase

    let mapleader=','
" }

" Formatting {
    set nowrap                      " don't wrap long lines
    " nvim - already default
    set autoindent                  " indent at the same level of the previous line
    set shiftwidth=2                " use indents of 2 spaces
    set shiftround                  " shift to a useful indentation
    set expandtab                   " tabs are spaces, not tabs
    set tabstop=2                   " set tab size
    set softtabstop=2               " let backspace delete indent
    "TODO: what does this do ?
    "set matchpairs+=<:>                " match, to be used with %
    "TODO: what does this do ?
    set pastetoggle=<F12>           " pastetoggle (sane indentation on pastes)
    "TODO: what does this do ?
    "set comments=sl:/*,mb:*,elx:*/  " auto format comment blocks
    " Remove trailing whitespaces and ^M chars
    " autocmd FileType c,cpp,java,php,javascript,python,twig,xml,yml autocmd BufWritePre <buffer> :call setline(1,map(getline(1,"$"),'substitute(v:val,"\\s\\+$","","")'))
    "TODO: what does this do ?
    autocmd BufNewFile,BufRead *.html.twig set filetype=html.twig
" }

" Plugins {
    call plug#begin('~/.vim/plugged')

    " Directory explorer
    Plug 'scrooloose/nerdtree'

    " Fuzzy Search
    Plug 'kien/ctrlp.vim'

    " Toggle comments
    Plug 'scrooloose/nerdcommenter'

    " Autocomplete
    Plug 'neoclide/coc.nvim'

    " Autocomplete for search bar
    Plug 'vim-scripts/SearchComplete'

    " Work with things like { }
    Plug 'tpope/vim-surround'

    " Can be used to swap different casing styles
    " TODO: Setup mappings for visual mode
    Plug 'tpope/vim-abolish'

    " Git
    Plug 'tpope/vim-fugitive'
    Plug 'airblade/vim-gitgutter'

    " Ctags
    " TODO: get this working
    Plug 'majutsushi/tagbar'

    " Aligning stuff like variable assignments
    Plug 'godlygeek/tabular'

    " Gives view of all open buffers
    Plug 'corntrace/bufexplorer'

    " Status line
    Plug 'vim-airline/vim-airline'

    " Visual undo explorer
    " TODO: Decide if this is actually useful
    Plug 'mbbill/undotree'

    " Show lines at indent levels
    Plug 'Yggdroot/indentLine'

    " Grep replacements
    " TODO: pick one of these
    Plug 'mileszs/ack.vim'
    Plug 'rking/ag.vim'

    " B)
    Plug 'Lokaltog/vim-easymotion'

    " Colors
    Plug 'daviddavis/vim-colorpack'
    Plug 'joshdick/onedark.vim'
    Plug 'nightsense/plumber'
    Plug 'beigebrucewayne/subtle_solo'
    Plug 'hzchirs/vim-material'
    Plug 'skielbasa/vim-material-monokai'

    " Language plugins
    Plug 'pangloss/vim-javascript'
    Plug 'mxw/vim-jsx'
    Plug 'kchmck/vim-coffee-script'
    Plug 'digitaltoad/vim-jade'
    Plug 'nono/vim-handlebars'
    Plug 'wavded/vim-stylus'
    Plug 'cakebaker/scss-syntax.vim'
    Plug 'alunny/pegjs-vim'
    Plug 'ifitzpatrick/vim-fbp'
    Plug 'vim-scripts/VimClojure'
    Plug 'jnwhiteh/vim-golang'
    Plug 'reasonml-editor/vim-reason-plus'
    Plug 'evanleck/vim-svelte'
    Plug 'leafgarland/typescript-vim'
    Plug 'peitalin/vim-jsx-typescript'
    Plug 'udalov/kotlin-vim'
    Plug 'vim-erlang/vim-erlang-runtime'

    " Linting
    Plug 'dense-analysis/ale'

    call plug#end()
" }

" Syntax {
    " add json syntax highlighting
    autocmd BufNewFile,BufRead *.json setlocal ft=javascript
" }

" Key Bindings {
    " Navigate between tabs with tab and shift tab
    nmap <Tab>      :tabn<CR>
    nmap <S-Tab>    :tabp<CR>
    " Open new tab with ,TAB
    nmap <Leader><Tab> :tabe<CR>

    nmap <Leader>ern :lnext<CR>
    nmap <Leader>erp :lprev<CR>
    nmap <Leader>erf :lfirst<CR>

    nmap <Leader>vs :BufExplorerVerticalSplit<CR>
    nmap <Leader>hs :BufExplorerHorizontalSplit<CR>
    nmap <Leader>nh :noh<CR>
    nmap <Leader>w :w<CR>

    vmap <Leader>sl :sort i<CR>

    nmap <Leader>bd :bdelete<CR>
    nmap <Leader>bn :bnext<CR>
    nmap <Leader>bp :bprevious<CR>

    nmap <Backspace> :e #<CR>

    nmap - ddkP
    nmap + ddp
    vmap - dkP`[V`]
    vmap + dp`[V`]

    " replace currently selected text with default register
    " without yanking it
    vnoremap <leader>p "_dP
" }

" File Type Settings {
    autocmd FileType python,vim setlocal tabstop=4 softtabstop=4 shiftwidth=4
    autocmd FileType python,coffee,jade setlocal foldmethod=indent
    autocmd BufEnter,BufNew *.cljs set ft=clojure
    autocmd BufEnter,BufNew *.md set ft=markdown
    autocmd BufEnter,BufNew *.peg set ft=pegjs
" }

" Plugin Settings {
    " Airline {
        if has ("unix") && "Darwin" != system("echo -n \"$(uname)\"")
            let g:airline_powerline_fonts = 1
            let g:airline_symbols_ascii = 1
        endif
    " }

    " COC {
        inoremap <expr> <Tab> pumvisible() ? "\<C-n>" : "\<Tab>"
        inoremap <expr> <S-Tab> pumvisible() ? "\<C-p>" : "\<S-Tab>"
        inoremap <expr> <cr> pumvisible() ? "\<C-y>" : "\<C-g>u\<CR>"
    " }

    " Tabularize {
        nmap <Leader>a= :Tabularize /=<CR>
        vmap <Leader>a= :Tabularize /=<CR>
        nmap <Leader>a- :Tabularize /-<CR>
        vmap <Leader>a- :Tabularize /-<CR>
        nmap <Leader>a: :Tabularize /:\zs/l0l1<CR>
        vmap <Leader>a: :Tabularize /:\zs/l0l1<CR>
        nmap <Leader>a, :Tabularize /,\zs/l0l1<CR>
        vmap <Leader>a, :Tabularize /,\zs/l0l1<CR>
        nmap <Leader>a$ :Tabularize /$\zs/l0l1<CR>
        vmap <Leader>a$ :Tabularize /$\zs/l0l1<CR>
    " }

    " Fugitive {
        nnoremap <leader>gs :Gstatus<CR>
        nnoremap <leader>gd :Gdiff<CR>
        nnoremap <leader>gc :Gcommit -a<CR>
        nnoremap <leader>gb :Gblame<CR>
        nnoremap <leader>gl :Git log -n 7<CR>
        nnoremap <leader>gp :Git push<CR>
        nnoremap <leader>ga :Git add %<CR>
     "}

     " Gitgutter {
        let g:gitgutter_set_sign_backgrounds = 0
     " }

    " Indent Guides {
        let g:indent_guides_enable_on_vim_startup = 0
        let g:indent_guides_color_change_percent = 30
        let g:indent_guides_auto_colors = 0
        autocmd VimEnter,Colorscheme * :hi IndentGuidesOdd guibg=#191919 ctermbg=3
        autocmd VimEnter,Colorscheme * :hi IndentGuidesEven guibg=#292929 ctermbg=4
    " }

    " Indent Lines {
        let g:indentLine_color_gui = '#333333'
    " }

    " Undotree {
        nmap <Leader>u :UndotreeToggle<CR>
    " }

    " Tagbar {
        nmap <Leader>t :TagbarToggle<CR>
    " }

    " NERDTree {
        let NERDTreeShowHidden=1
        let NERDTreeIgnore=['\.pyc$']
        nmap <Leader>nt :NERDTreeToggle<CR>
        nmap <Leader>nf :NERDTreeFind<CR>
    " }

    " CtrlP {
        nmap <Leader><c-p> :CtrlPClearCache<CR>
        nmap <Leader>pf :CtrlP<CR>
        let g:ctrlp_custom_ignore = {
            \ 'dir': 'node_modules$'
            \ }
        let g:ctrlp_reuse_window = '[BufExplorer]'
    " }

    " CoffeeScript {
        " coffeescript not installed globally at work
        if $AT_LUMA == 1
          let g:coffee_compiler = '~/Code/coffeescript/bin/coffee'
        endif
    " }

    " JSX {
        let g:jsx_ext_required = 0
    " }

    " BufExplorer {
        let g:bufExplorerShowRelativePath=1
    " }

    " ALE{
        let g:ale_linters = {
        \'javascript': ['flow', 'eslint'],
        \}
        let g:ale_fixers = {
        \'javascript': ['eslint'],
        \}
    " }

    " VIM-Javascript {
        let g:javascript_plugin_flow = 1
    " }
" }

" Vim UI {
    " nvim - already default
    set showmode                    " display the current mode

    set cursorline                  " highlight current line

    if has('cmdline_info')
        set ruler                   " show the ruler
        set rulerformat=%30(%=\:b%n%y%m%r%w\ %l,%c%V\ %P%) " a ruler on steroids
        set showcmd                 " show partial commands in status line and
                                    " selected characters/lines in visual mode
    endif

    if has('statusline')
        set laststatus=2

        " Broken down into easily includeable segments
        set statusline=%<%f\    " Filename
        set statusline+=%w%h%m%r " Options
        set statusline+=%{FugitiveStatusline()} "  Git Hotness
        set statusline+=\ [%{&ff}/%Y]            " filetype
        set statusline+=\ [%{getcwd()}]          " current dir
        set statusline+=%=%-14.(%l,%c%V%)\ %p%%  " Right aligned file nav info
    endif

    " nvim - already default
    set backspace=indent,eol,start  " backspace for dummies
    " nvim - already default
    set linespace=0                 " No extra spaces between rows
    set nu                          " Line numbers on
    set showmatch                   " show matching brackets/parenthesis
    set winminheight=0              " windows can be 0 line high
    " nvim - already default
    set wildmenu                    " show list instead of just completing
    set wildmode=list:longest,full  " command <Tab> completion, list matches, then longest common part, then all.
    set whichwrap=b,s,h,l,<,>,[,]   " backspace and cursor keys wrap to
    " set scrolljump=5                " lines to scroll when cursor leaves screen
    set scrolloff=8                 " minimum lines to keep above and below cursor
    set foldenable                  " auto fold code
    set foldmethod=syntax
    set foldlevelstart=100
    set list
    set listchars=tab:\ \ ,trail:.,extends:#,nbsp:. " Highlight problematic whitespace
    " nvim - already default
    set showcmd
    if !has("nvim")
        set noesckeys
    endif


    set relativenumber
    au InsertEnter * set norelativenumber
    au InsertLeave * set relativenumber


    if has("gui_running")
        " Highlight area past 80 characters
        augroup vimrc_autocmds
            autocmd BufEnter * let &colorcolumn=join(range(81, 100), ",")
            autocmd BufEnter * hi ColorColumn guibg=#101010
            autocmd BufEnter * hi ColorColumn ctermbg=8
        augroup end
    endif
" }

" gvim {
    " Default gui color scheme
    if has("gui_running")
        if g:os == "Darwin" || g:os == "mac"
            set guifont=monaco:h22
        else
            set guifont=Monaco\ 16
        endif

        color ir_black
    endif

    highlight SignColumn guibg=#111111
" }

" FUN FUN FUNCTIONS {
    function! CountLines() range
        echo 1 + getpos("'>")[1] - getpos("'<")[1]
    endfunction

    function! _CompressLines(begin, end)
        let placeholder = "-----COMPRESS-----"
        execute a:begin . "," . a:end . "s/\\n/" . placeholder . "/g"
    endfunction

    function! _ExpandLines(line_range)
        let placeholder = "-----COMPRESS-----"
        execute a:line_range . "s/" . placeholder . "/\\r/g"
    endfunction

    function! CompressLines() range
        let begin = getpos("'<")[1]
        let end = getpos("'>")[1] - 1

        call _CompressLines(begin, end)
    endfunction

    function! ExpandLine()
        let line = getpos("'<")[1]
        call _ExpandLines(line)
    endfunction

    function! SortGroups(n) range
        let begin = getpos("'<")[1]
        let end = getpos("'>")[1]

        let steps = (1 + end - begin) / a:n
        let current = 0

        while current < steps
            let alter_begin = begin + current
            let alter_end = begin + current + a:n - 2
            call _CompressLines(alter_begin, alter_end)

            let current += 1
        endwhile

        let sort_range = begin . "," . (begin + steps - 1)

        execute sort_range . "sort"
        call _ExpandLines(sort_range)

        noh
        call cursor(begin,0)
        normal! V
        call cursor(end,0)
    endfunction

    vnoremap <Leader>uc :call CompressLines()<CR>
    vnoremap <Leader>ue :call ExpandLine()<CR>

    function! Scratch()
        :vsplit __vim__scratch
        :setlocal buftype=nofile
        :setlocal bufhidden=hide
        :setlocal noswapfile
    endfunction

    function! DirBuffer()
        :vsplit __vim__dirbuffer
        :setlocal buftype=nofile
        :setlocal bufhidden=hide
        :setlocal noswapfile

        call append(line(''), [getcwd()])

        :setlocal nomodifiable
    endfunction

    noremap <Leader>us :call Scratch()<CR>
" }
