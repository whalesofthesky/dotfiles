;; This is only needed once, near the top of the file
(eval-when-compile
  (require 'use-package))

(require 'use-package-ensure)
(setq use-package-always-ensure t)

(require 'package)
(add-to-list 'package-archives
             '("melpa" . "http://melpa.org/packages/") t)

(defun ian-resolve-file-name (filename)
  ""
  (file-truename (expand-file-name filename)))

;; Shell
(use-package exec-path-from-shell
  :defer 0.1
  :config
  (when (daemonp)
    (exec-path-from-shell-initialize)))

(defun ian-persp-vterm ()
  "Open a vterm buffer paired with current perspective."
  (interactive)
  (vterm (concat "*vterm " (persp-current-name) "*")))

;; Clean Folders
(setq backup-directory-alist
  `(("." . ,(expand-file-name "tmp/backups" user-emacs-directory))))

(make-directory (expand-file-name "tmp/auto-saves" user-emacs-directory) t)

(setq auto-save-list-file-prefix
  (expand-file-name "tmp/auto-saves/sessions/" user-emacs-directory)
  auto-save-file-name-transforms
  `((".*" ,(expand-file-name "tmp/auto-saves/" user-emacs-directory) t)))

; Not really necessary to do this since I keep my actual stuff separate from ~/.emacs.d
(setq projectile-known-projects-file
  (expand-file-name "tmp/projectile-bookmarks.eld" user-emacs-directory))

; these prevent multiple emacs's from colliding
; had to turn this off because these files were causing ess/R to break
(setq create-lockfiles nil)

(defun ian-load-lib (lib)
  "Load some LIB"
  (load-file (concat user-emacs-directory "libv2/" lib ".el")))

(ian-load-lib "org")
(ian-load-lib "search")
(ian-load-lib "workspace")
(ian-load-lib "shell-helpers")

(use-package helpful
  :commands (helpful-callable helpful-command helpful-variable helpful-key)
  :bind
  ([remap describe-function] . helpful-callable)
  ([remap describe-command] . helpful-command)
  ([remap describe-variable] . helpful-variable)
  ([remap describe-key] . helpful-key))

;; Keys
(ian-load-lib "keys")

;; UI
(use-package doom-themes
  :config
  (load-theme 'doom-henna t))

(use-package doom-modeline
  :after doom-themes
  :init (doom-modeline-mode 1)
  :custom
  (doom-modeline-height 40)
  (doom-modeline-icon t))

(use-package nerd-icons)
  ;:config (nerd-icons-install-fonts))

(use-package ace-window
  :commands (ace-window)
  :custom
  (aw-scope 'frame))

(defun ian-split-window-right ()
  "Split window right w/buffer menu."
  (interactive)
  (split-window-right)
  (ian-ibuffer))

(defun ian-split-window-below ()
  "Split window below w/buffer menu."
  (interactive)
  (split-window-below)
  (ian-ibuffer))

(defun ian-edit-init ()
  "Edit ~/.emacs.d/init.el."
  (interactive)
  (when (not (eq (persp-current-name) "dotfiles")) 
    (persp-switch "dotfiles"))
  (find-file (concat user-emacs-directory "init.el")))

(use-package all-the-icons
  :diminish
  :config
  (unless (member "all-the-icons" (font-family-list))
    (all-the-icons-install-fonts t)))

(use-package all-the-icons-dired
  :diminish
  :config
    (add-hook 'dired-mode-hook 'all-the-icons-dired-mode))

(defun ian-setup-fonts (&optional font-size)
  ""
  (interactive "nFont size: ")
  (setq ian-default-font-size (or font-size 135))
  (setq ian-default-font-mono "JetBrains Mono")
  ;; (setq ian-default-font-mono "Monaco")
  (setq ian-default-font-variable "Lato")
  (setq ian-default-font-weight 'light)
  (setq ian-default-font-width 'wide)

  (set-face-attribute 'default nil
                      :font ian-default-font-mono
                      :height ian-default-font-size
                      :width ian-default-font-width
                      :weight ian-default-font-weight
                      :slant 'normal)
  (set-face-attribute 'fixed-pitch nil
                      :font ian-default-font-mono
                      :height ian-default-font-size
                      :weight ian-default-font-weight
                      :width ian-default-font-width
                      :slant 'normal)
  (set-face-attribute 'variable-pitch nil
                      :font ian-default-font-variable
                      :height ian-default-font-size
                      :width ian-default-font-width
                      :weight ian-default-font-weight))

(ian-setup-fonts 150)

(menu-bar-mode -1)
(tool-bar-mode -1)
(show-paren-mode)
(setq visible-bell nil) ; instead of audio bell
(setq vc-follow-symlinks t)
(setq inhibit-startup-screen t)

(use-package typescript-mode
  :commands typescript-mode
  :mode "\\.ts$"
  :config
  ; TODO: Can move this to indent.el?
  (setq typescript-indent-level 2))

(use-package markdown-mode
  :commands markdown-mode)

(use-package ox-hugo
  :ensure t   ;Auto-install the package from Melpa
  :pin melpa  ;`package-archives' should already have ("melpa" . "https://melpa.org/packages/")
  :after ox)

(setq ian-local-config "~/Code/system/local/local.el")
(if (file-exists-p ian-local-config)
    (load-file ian-local-config)
  (message "Local config does not exist."))

(custom-set-variables
 ;; custom-set-variables was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 '(package-selected-packages
   '(ace-window all-the-icons-dired consult copilot-chat diminish
		doom-modeline doom-themes eglot-java evil-collection
		evil-easymotion evil-org evil-surround
		exec-path-from-shell fennel-mode fontawesome general
		helpful highlight-indent-guides kotlin-mode magit
		marginalia markdown-mode orderless org-roam
		ox-awesomecv ox-hugo persp-mode perspective
		projectile-ripgrep typescript-mode vertico vterm
		which-key)))
(custom-set-faces
 ;; custom-set-faces was added by Custom.
 ;; If you edit it by hand, you could mess it up, so be careful.
 ;; Your init file should contain only one such instance.
 ;; If there is more than one, they won't work right.
 )
