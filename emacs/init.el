(defvar bootstrap-version)
(let ((bootstrap-file
       (expand-file-name "straight/repos/straight.el/bootstrap.el" user-emacs-directory))
      (bootstrap-version 5))
  (unless (file-exists-p bootstrap-file)
    (with-current-buffer
        (url-retrieve-synchronously
         "https://raw.githubusercontent.com/raxod502/straight.el/develop/install.el"
         'silent 'inhibit-cookies)
      (goto-char (point-max))
      (eval-print-last-sexp)))
  (load bootstrap-file nil 'nomessage))

(straight-use-package 'use-package)
(setq straight-use-package-by-default t)

(use-package exec-path-from-shell
  :config
  (when (daemonp)
    (exec-path-from-shell-initialize)))

(defun ian-load-lib (lib)
  "Load some LIB"
  (load-file (concat user-emacs-directory "lib/" lib ".el")))

(defvar ian-config-org-roam-dailies nil)
(defvar ian-config-company t)

;; Setup Custom file
(setq custom-file (concat user-emacs-directory "custom.el"))
(load-file custom-file)

(ian-load-lib "lib")

(ian-load-lib "browse")
(ian-load-lib "clean-folders")
(ian-load-lib "dired")
(ian-load-lib "indent")
(ian-load-lib "lang")
(ian-load-lib "git")
(ian-load-lib "org")
(ian-load-lib "search")
(ian-load-lib "term")
(ian-load-lib "text-tools")
(ian-load-lib "ui")

(ian-load-lib "keys")

; When running with systemd we need to get env vars from the right shell
(defun ian-env-get (env)
  ""
  (substring (shell-command-to-string (concat "echo " env)) 0 -1))

(defun ian-env-dir-get (env)
  ""
  (concat (ian-env-get env) "/"))

; Local config - personal or work config under source control
(defvar ian-env-local (ian-env-dir-get "$IAN_LOCAL"))
(defvar ian-env-local-init (concat ian-env-local "emacs/init.el"))
(when (file-exists-p ian-env-local-init)
  (load-file ian-env-local-init))

; Home config - Machine specific config not under source control
(defvar ian-env-home (ian-env-dir-get "$IAN_SYSTEM_CONFIG"))
(defvar ian-env-home-init (concat ian-env-home "emacs/init.el"))
(when (file-exists-p ian-env-home-init)
  (load-file ian-env-home-init))

;(setq gc-cons-threshold 100000000)
(setq gc-cons-threshold (* 2 1000 1000))
(setq read-process-output-max (* 1024 1024))

(setq tramp-default-method "ssh")
; recent files
(recentf-mode 1)
; Fix symlink files names in modeline
(setq find-file-visit-truename t)
