;;; emenu.el --- Create a rofi-like menu -*- lexical-binding: t -*-

(defun emenu ()
  ""
  (let ((buffer (get-buffer-create "Emenu")))
    (let ((frame (make-frame `((name . "(Emenu)")))))
      (with-selected-frame frame
        (switch-to-buffer buffer)
        (with-current-buffer buffer
          (insert "Emenu!!!")
          (setq-local mode-line-format nil)
          (setq-local max-mini-window-height 1.0)
          (enlarge-window 0 500)
          (counsel-load-theme))))))
        
(add-to-list 'display-buffer-alist '("Emenu" . display-buffer-same-window))
(emenu)

      ;; (completing-read "lolwtuf" '(omg lol wtf bro))
