(setq package-enable-at-startup nil)
(setq gc-cons-threshold (* 50 1000 1000))
(setq comp-async-report-warnings-errors nil)
