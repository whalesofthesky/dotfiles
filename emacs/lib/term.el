;; -*- lexical-binding: t -*-
(use-package term
  :config
  (setq explicit-shell-file-name "zsh"))

; depends on `tic` command from ncurses
(use-package eterm-256color
  :hook (term-mode . eterm-256color-mode))

(defun ian-vterm-insert-setup ()
  "Reset point when entering insert mode in vterm."
  (interactive)
  (add-hook 'evil-insert-state-entry-hook
            'vterm-reset-cursor-point
            nil t))

(use-package vterm
  :commands (vterm)
  :init
  (add-hook 'vterm-mode-hook #'ian-vterm-insert-setup)
  :config
  (setq vterm-timer-delay 0.1)
  (setq vterm-max-scrollback 10000))

; https://github.com/daviwil/dotfiles/blob/master/Emacs.org#eshell
(defun dw/get-prompt-path ()
  (let* ((current-path (eshell/pwd))
         (git-output (shell-command-to-string "git rev-parse --show-toplevel"))
         (has-path (not (string-match "^fatal" git-output))))
    (if (not has-path)
      (abbreviate-file-name current-path)
      (string-remove-prefix (file-name-directory git-output) current-path))))

(defun dw/eshell-prompt ()
  (let ((current-branch (magit-get-current-branch)))
    (concat
     "\n"
     (propertize (system-name) 'face `(:foreground "#62aeed"))
     (propertize " ॐ " 'face `(:foreground "white"))
     (propertize (dw/get-prompt-path) 'face `(:foreground "#82cfd3"))
     (when current-branch
       (concat
        (propertize " • " 'face `(:foreground "white"))
        (propertize (concat " " current-branch) 'face `(:foreground "#c475f0"))))
     (propertize " • " 'face `(:foreground "white"))
     (propertize (format-time-string "%I:%M:%S %p") 'face `(:foreground "#5a5b7f"))
     (if (= (user-uid) 0)
         (propertize "\n#" 'face `(:foreground "red2"))
       (propertize "\nλ " 'face `(:foreground "#aece4a"))))))

(setq eshell-prompt-function 'dw/eshell-prompt
      eshell-prompt-regexp "^λ ")

(defun ian-async-shell-command (command)
  ""
  (interactive "sCommand: ")
  (let* ((buffer-name (concat "*Async Command<" command ">*"))
         (buffer-process (get-buffer-process buffer-name)))
    (if (not buffer-process)
        (async-shell-command command buffer-name)
      (when (y-or-n-p "Process already running. Restart? ")
        (ian-async-shell-command--on-process-killed
         buffer-process
         `(lambda () (async-shell-command ,command ,buffer-name)))
        (kill-process buffer-process)))))

(defun ian-async-shell-command--buffer-select ()
  ""
  (completing-read
   "Buffer to kill: "
   (seq-filter (lambda (s) (s-starts-with-p "*Async" s))
               (seq-map #'buffer-name (buffer-list)))))

(defun ian-async-shell-command--on-process-killed (process callback)
  ""
  (set-process-sentinel
   process
   (lambda (process status)
      (when (equal status "killed\n")
        (callback)))))
  
(defun ian-async-shell-command-switch-to-buffer ()
  ""
  (interactive)
  (let* ((name (ian-async-shell-command--buffer-select))
         (buffer (get-buffer name)))
    (switch-to-buffer buffer)))

(defun ian-async-shell-command-switch-to-buffer-other-window ()
  ""
  (interactive)
  (let* ((name (ian-async-shell-command--buffer-select))
         (buffer (get-buffer name)))
    (switch-to-buffer-other-window buffer)))

(defun ian-async-shell-command-kill ()
  ""
  (interactive)
  (let* ((name (completing-read
                "Buffer to kill: "
                (seq-filter (lambda (s) (s-starts-with-p "*Async" s))
                            (seq-map #'buffer-name (buffer-list)))))
         (buffer (get-buffer name))
         (buffer-process (get-buffer-process buffer)))
    (if (not buffer-process)
        (kill-buffer buffer)
      (ian-async-shell-command--on-process-killed
       buffer-process
       `(lambda () (kill-buffer ,buffer)))
      (kill-process buffer-process))))
