(use-package diminish)

(ian-load-lib "search/vertico")

(use-package which-key
  :defer 0.1
  :init  (which-key-mode)
  :diminish which-key-mode
  :config
  (setq which-key-idle-delay 0.5))

(use-package projectile-ripgrep :after projectile)

(use-package projectile
  :defer 0.1
  :diminish projectile-mode
  ;:custom ((projectile-completion-system 'selectrum))
  ;; :bind (("C-p" . projectile-find-file))
  :init
  (setq projectile-switch-project-action #'projectile-dired)
  :config
  (projectile-mode)
  ; Add individual projects
  (dolist (project (append
    (file-expand-wildcards "~/Code/learn/lang/*/notes")
    (file-expand-wildcards "~/Code/learn/platform/*/notes")
    '("~/Documents/health"
      "~/Documents/org"
      "~/Code/learn/platform/emacs"
      "~/Code/learn/lang/c")))
    (projectile-add-known-project (expand-file-name project)))
  ; Add project dirs
  (dolist (project-dir '("~/Code/project/active"
                         "~/Code/project/active/multiplayer-clj-test"
                         "~/Code/oss/emacs"
                         "~/Code/system"))
    (projectile-discover-projects-in-directory (expand-file-name project-dir))))

(setq-default company-lsp-cache-candidates 'auto)

(defun ian-corfu-quit () (interactive) (message "lolololol"))

(if ian-config-company
    (progn
      (use-package company
        :defer 0.1
        :diminish
        :custom
        (company-idle-delay 0.1)
        ; https://emacs.stackexchange.com/questions/10837/how-to-make-company-mode-be-case-sensitive-on-plain-text
        (company-dabbrev-downcase nil)
        :config
        (global-company-mode))

      ; This thing is buggy
      ; (use-package company-box
      ;   :after company
      ;   :hook (company-mode . company-box-mode))

      (use-package company-prescient
        :after company
        :diminish
        :config
        (company-prescient-mode)))
  (use-package corfu
    :disabled
    :after evil
    :custom
    (corfu-auto t)
    (corfu-cycle t)
    (corfu-preview-current nil) ;; Disable current candidate preview
    (corfu-preselect-first nil) ;; Disable candidate preselection
    :bind
    (:map corfu-map
          ("TAB" . corfu-next)
          ([tab] . corfu-next)
          ("S-TAB" . corfu-previous)
          ([backtab] . corfu-previous))
    :init
    (evil-define-key '(normal) corfu-map (kbd "<escape>") #'corfu-quit)
    (message "this is happening")
    (corfu-global-mode t))
  (use-package cape
    :straight (cape
               :host github
               :repo "minad/cape")
    ;; Bind dedicated completion commands
    ;; :bind (("C-c p p" . completion-at-point) ;; capf
    ;;        ("C-c p t" . complete-tag)        ;; etags
    ;;        ("C-c p d" . cape-dabbrev)        ;; or dabbrev-completion
    ;;        ("C-c p f" . cape-file)
    ;;        ("C-c p k" . cape-keyword)
    ;;        ("C-c p s" . cape-symbol)
    ;;        ("C-c p a" . cape-abbrev)
    ;;        ("C-c p i" . cape-ispell)
    ;;        ("C-c p l" . cape-line)
    ;;        ("C-c p w" . cape-dict)
    ;;        ("C-c p \\" . cape-tex)
    ;;        ("C-c p &" . cape-sgml)
    ;;        ("C-c p r" . cape-rfc1345))
    :init
    ;; Add `completion-at-point-functions', used by `completion-at-point'.
    (add-to-list 'completion-at-point-functions #'cape-file)
    (add-to-list 'completion-at-point-functions #'cape-tex)
    (add-to-list 'completion-at-point-functions #'cape-dabbrev)
    (add-to-list 'completion-at-point-functions #'cape-keyword)
    ;;(add-to-list 'completion-at-point-functions #'cape-sgml)
    ;;(add-to-list 'completion-at-point-functions #'cape-rfc1345)
    ;;(add-to-list 'completion-at-point-functions #'cape-abbrev)
    ;;(add-to-list 'completion-at-point-functions #'cape-ispell)
    ;;(add-to-list 'completion-at-point-functions #'cape-dict)
    ;;(add-to-list 'completion-at-point-functions #'cape-symbol)
    ;;(add-to-list 'completion-at-point-functions #'cape-line)
    )
  )

(use-package yasnippet
  :defer 0.1
  :diminish
  :config
  (yas-global-mode))

(use-package yasnippet-snippets
  :after yasnippet)

(use-package perspective
  :defer 0.1
  :diminish persp-mode
  :custom
  ((persp-show-modestring nil))
  :config
  (unless persp-mode
    (persp-mode 1)))
