(use-package dired-x
  :after (general)
  :ensure nil
  :straight nil
  :custom
  (dired-omit-files "^\\...+$")
  :config
  (general-define-key
   :states '(normal motion visual)
   :keymaps '(
              dired-mode-map)
   "SPC" nil)
  (setq global-auto-revert-non-file-buffers t)
  (advice-add 'dired-subtree-toggle :after (lambda (&rest args) (revert-buffer))))

(use-package ranger :commands ranger)
