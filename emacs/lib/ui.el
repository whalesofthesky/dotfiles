(defun ian-setup-fonts (&optional font-size)
  ""
  (interactive "nFont size: ")
  (setq ian-default-font-size (or font-size 135))
  (setq ian-default-font-mono "JetBrains Mono")
  ;; (setq ian-default-font-mono "Monaco")
  (setq ian-default-font-variable "Lato")
  (setq ian-default-font-weight 'light)
  (setq ian-default-font-width 'wide)

  (set-face-attribute 'default nil
                      :font ian-default-font-mono
                      :height ian-default-font-size
                      :width ian-default-font-width
                      :weight ian-default-font-weight
                      :slant 'normal)
  (set-face-attribute 'fixed-pitch nil
                      :font ian-default-font-mono
                      :height ian-default-font-size
                      :weight ian-default-font-weight
                      :width ian-default-font-width
                      :slant 'normal)
  (set-face-attribute 'variable-pitch nil
                      :font ian-default-font-variable
                      :height ian-default-font-size
                      :width ian-default-font-width
                      :weight ian-default-font-weight))

(ian-setup-fonts 150)

(setq line-spacing 0.1)

(menu-bar-mode -1)
(tool-bar-mode -1)

;(global-display-line-numbers-mode)
; (setq-default display-line-numbers-type 'visual)
; (setq-default display-line-numbers-grow-only t)

(use-package nlinum :commands nlinum-mode)

(use-package nlinum-relative
    :config
    ;; something else you want
    (nlinum-relative-setup-evil)
    (add-hook 'prog-mode-hook 'nlinum-relative-mode))

(show-paren-mode)

(setq visible-bell nil) ; instead of audio bell
(setq vc-follow-symlinks t)
(setq inhibit-startup-screen t)
;; (add-to-list 'default-frame-alist '(fullscreen . maximized))
(dolist (mode '(vterm-mode-hook
                term-mode-hook
		eshell-mode-hook
		dired-sidebar-mode-hook))
  (add-hook mode (lambda ()
                   (display-line-numbers-mode 0)
                   (setq display-line-numbers nil))))

;; Theming
(defun ian-bg-color ()
  "Get bg color."
  (nth 1 (assq 'bg doom-themes--colors)))

(defun ian-bg-color-lighten (percent)
  "Ligthen bg color by PERCENT."
  (color-lighten-name (ian-bg-color) percent))

(defun ian-bg-color-darken (percent)
  "Ligthen bg color by PERCENT."
  (doom-darken (ian-bg-color) percent))

(defun ian-update-ui-colors ()
  "Update ui colors based on theme."
  (interactive)
  (set-face-attribute
   'fill-column-indicator nil
   :background (doom-darken (nth 1 (assq 'bg doom-themes--colors)) 2)
   :extend t))
  ;(when (custom-facep 'highlight-indent-guides-even-face)
  ;  (set-face-attribute 'highlight-indent-guides-even-face nil
  ;                      :background (ian-bg-color-lighten 4))
  ;  (set-face-attribute 'highlight-indent-guides-odd-face nil
  ;                      :background (ian-bg-color))))

(defun ian-doom-themes-setup ()
  ""
  (interactive)
  (load-theme 'doom-henna t)
  (ian-update-ui-colors))

(use-package doom-themes
  :straight (doom-themes
             :host github
             :repo "ifitzpatrick/emacs-doom-themes")
  :config
  (ian-doom-themes-setup))

; For some reason colors are messed up in daemon mode if I don't do this
(setq ian-doom-loaded nil)
(add-hook 'server-after-make-frame-hook
          (lambda ()
            (unless ian-doom-loaded
              (setq ian-doom-loaded t)
              (ian-doom-themes-setup))))

(use-package doom-modeline
  :after doom-themes
  :init (doom-modeline-mode 1)
  :custom
  (doom-modeline-height 40)
  (doom-modeline-icon t))

(use-package rainbow-delimiters
  :hook (prog-mode . rainbow-delimiters-mode))

(use-package dired-sidebar)

(use-package all-the-icons
  :diminish
  :config
  (unless (member "all-the-icons" (font-family-list))
    (all-the-icons-install-fonts t)))

(use-package all-the-icons-dired
  :diminish
  :config
    (add-hook 'dired-mode-hook 'all-the-icons-dired-mode))

(add-hook 'prog-mode-hook
  (lambda () (setq truncate-lines t)))

(column-number-mode)

(scroll-bar-mode 0)
(setq scroll-margin 8)
(setq scroll-conservatively 8)

;(add-hook 'buffer-list-update-hook
;  (lambda ()
;    (let* ((window (selected-window))
;           (width (nth 0 (window-scroll-bars)))
;           (lines (count-lines (point-min) (point-max)))
;           (height (window-height window)))
;      ; For some reason this gets unset only in emacsclient
;      ; otherwise, only need to run this once on startup
;      (set-window-scroll-bars (minibuffer-window) 0)
;      (if (or (<= lines height)
;              (eq (minibuffer-window) window))
;          (unless (eq width 0)
;            (set-window-scroll-bars window 0))
;        (unless (eq width nil)
;          (set-window-scroll-bars window nil t nil nil t))))))

(window-divider-mode)

;(defface ian-long-lines-marker2 '((t (:background "black")))
;  "Face used for a column marker.  Usually a background color."
;  :group 'ian-marker)
;
;(defface ian-long-lines-marker '((t (:background "black")))
;  "Face used for a column marker.  Usually a background color."
;  :group 'ian-marker)
;
;(setq ovr (make-overlay 2185 2414))
;
;(overlay-put ovr 'face 'ian-long-lines-marker2)
;
;(move-overlay ovr 2200 2300)
;
;(overlay-put ovr 'invisible nil)
;
;(overlay-start ovr)
;
;(delete-overlay ovr)

; (overlay-put myoverlay3 'after-string (concat "     " (propertize "    " 'face hl-line-face)))

(setq-default fill-column 80)

(setq-default display-fill-column-indicator-character ? )
(setq-default display-fill-column-indicator-column (+ (default-value 'fill-column) 1))

(add-hook 'prog-mode-hook
          (lambda ()
            (display-fill-column-indicator-mode)
            (when (require 'doom-themes nil 'noerror)
              (ian-update-ui-colors))))

;(use-package highlight-indent-guides
;  :hook (prog-mode . highlight-indent-guides-mode)
;  :custom
;  (highlight-indent-guides-auto-enabled nil))

(use-package pdf-tools
  :disabled
  :straight '(pdf-tools :build (:not compile))
  :config
  (pdf-loader-install))

(use-package alert
  :commands (alert)
  :custom
  (alert-default-style 'osx-notifier)
  (alert-fade-time 3))

(use-package origami)

(setq browse-url-chrome-program "google-chrome-stable")

(defun browse-url-chrome (url &optional new-window)
  "Ask the Google Chrome WWW browser to load URL.
Default to the URL around or before point.  The strings in
variable `browse-url-chrome-arguments' are also passed to
Google Chrome.
The optional argument NEW-WINDOW IS used."
  (interactive (browse-url-interactive-arg "URL: "))
  (setq url (browse-url-encode-url url))
  (let* ((process-environment (browse-url-process-environment)))
    (apply #'start-process
	   (concat "google-chrome " url ) nil
	   browse-url-chrome-program
	   (append
	    browse-url-chrome-arguments
            (if new-window
                (list "--new-window")
              nil)
	    (list url)))))

; https://github.com/jcaw/theme-magic
; (use-package theme-magic)

(use-package ace-window
  :commands (ace-window)
  :custom
  (aw-scope 'frame))
