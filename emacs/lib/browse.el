(defun ian-google (arg)
  ""
  (interactive "sQuery: ")
  (browse-url (concat "https://www.google.com/search?q=" (url-encode-url arg))))

(defun ian--wikipedia-url (query)
  ""
  (concat "https://en.wikipedia.org/w/index.php?search=" query "&title=Special:Search&go=Go&ns0=1"))

(defun ian-wikipedia-eww (query)
  "Open wikipedia page with eww."
  (interactive "sQuery: ")
  (eww (ian--wikipedia-url query)))

(defun ian-wikipedia (query)
  ""
  (interactive "sQuery: ")
  (browse-url (ian--wikipedia-url query)))

; Not sure where else to put this
(setq browse-url-browser-function 'browse-url-generic
	browse-url-generic-program "google-chrome-stable")
