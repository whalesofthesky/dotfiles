(use-package magit
  :ensure t
  ; magit-get-current-branch used for eshell prompt
  :commands (magit-blame magit-diff magit-status magit-get-current-branch)
  ; :hook (magit-revision-mode . evil-collection-magit-toggle-text-mode)
  :custom
  (magit-display-buffer-function #'magit-display-buffer-same-window-except-diff-v1)
  (magit-blame-styles
   '(
     (margin
      (margin-format " %s%f" " %C %a" " %H")
      (margin-width . 42)
      (margin-face . magit-blame-margin)
      (margin-body-face magit-blame-dimmed))
     (headings
       (heading-format . "%-20a %C %s\n"))))
  :config
  (advice-add 'magit-push-current-to-upstream
              :override (lambda (&rest args)
                          (error "Don't push directly to upstream!!!"))))

(use-package magit-section)

(use-package diff-hl
  :config (global-diff-hl-mode))

; This thing is really slow for some reason
; (use-package git-gutter
;   :config
;   (global-git-gutter-mode))
