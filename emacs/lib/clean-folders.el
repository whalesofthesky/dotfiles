(setq backup-directory-alist
  `(("." . ,(expand-file-name "tmp/backups" user-emacs-directory))))

(make-directory (expand-file-name "tmp/auto-saves" user-emacs-directory) t)

(setq auto-save-list-file-prefix
  (expand-file-name "tmp/auto-saves/sessions/" user-emacs-directory)
  auto-save-file-name-transforms
  `((".*" ,(expand-file-name "tmp/auto-saves/" user-emacs-directory) t)))

; Not really necessary to do this since I keep my actual stuff separate from ~/.emacs.d
(setq projectile-known-projects-file
  (expand-file-name "tmp/projectile-bookmarks.eld" user-emacs-directory))

; these prevent multiple emacs's from colliding
; had to turn this off because these files were causing ess/R to break
(setq create-lockfiles nil)

(provide 'clean-folders)

; https://github.com/emacscollective/no-littering
; (use-package no-littering)
