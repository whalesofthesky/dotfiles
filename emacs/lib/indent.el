; Mostly followed this guide to configuring tabs
; https://dougie.io/emacs/indentation/
; double semicolon (;;) comments from original author

;; Create a variable for our preferred tab width
(setq custom-tab-width 2)
(setq tab-width 2)

(setq-default indent-tabs-mode nil)

;; Language-Specific Tweaks
(use-package python
  :custom
  (python-indent-offset 4))

(setq-default css-indent-offset custom-tab-width)
(setq-default js-indent-level custom-tab-width)      ;; Javascript
(setq-default standard-indent custom-tab-width)
(setq-default web-mode-style-padding custom-tab-width)
(setq-default web-mode-script-padding custom-tab-width)

;; Making electric-indent behave sanely
(setq-default electric-indent-inhibit t)

;; Make the backspace properly erase the tab instead of
;; removing 1 space at a time.
(setq backward-delete-char-untabify-method 'hungry)

;; (OPTIONAL) Shift width for evil-mode users
;; For the vim-like motions of ">>" and "<<".
(setq-default evil-shift-width custom-tab-width)

; FIXME: Figure out how to turn this off in dired etc.
;; WARNING: This will change your life
;; (OPTIONAL) Visualize tabs as a pipe character - "|"
;; This will also show trailing characters as they are useful to spot.
; (setq-default whitespace-style '(face tabs tab-mark trailing))
; (custom-set-faces
;  '(whitespace-tab ((t (:foreground "#636363")))))
;(setq-default whitespace-display-mappings
;  '((tab-mark 9 [124 9] [92 9]))) ; 124 is the ascii ID for '\|'
;(global-whitespace-mode)

; FIXME: not working for some reason
(add-hook 'vterm-mode-hook
  (lambda ()
    (whitespace-mode 0)))

; Additional config for webmode that has its own weird indent management system
(setq-default web-mode-code-indent-offset 2)
(setq-default web-mode-css-indent-offset 2)
(setq-default web-mode-markup-indent-offset 2)
