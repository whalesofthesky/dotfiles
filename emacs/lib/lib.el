(defun ian/get (key-list list)
  "Follow KEY-LIST to get property out of LIST."
  (if (and key-list list)
      (ian/get (cdr key-list) (assq (car key-list) list))
    (cdr list)))

(defun ian/read-file (name)
  "Read file NAME into string."
  (with-temp-buffer
    (insert-file-contents name)
    (buffer-string)))

(defun ian/advice-unadvice (sym)
  "Remove all advices from symbol SYM."
  (interactive "aFunction symbol: ")
  (advice-mapc (lambda (advice _props) (advice-remove sym advice)) sym))

(defun ian-resolve-file-name (filename)
  ""
  (file-truename (expand-file-name filename)))
