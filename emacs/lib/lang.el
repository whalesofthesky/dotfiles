(use-package lsp-mode
  :diminish lsp-mode
  :ensure nil
  :commands (lsp lsp-deferred)
  :custom ((lsp-keymap-prefix "C-c l")
           (lsp-eslint-quiet t)
           (lsp-eslint-save-library-choices t)
           (lsp-headerline-breadcrumb-enable nil)
           (lsp-modeline-code-actions-enable nil)
           (lsp-modeline-diagnostics-enable nil)
           (lsp-modeline-workspace-status-enable nil)
           (lsp-enable-file-watchers nil))
  :config
  (lsp-enable-which-key-integration t))

(use-package lsp-ui
  :after lsp-mode
  :diminish lsp-ui-mode
  :commands lsp-ui-mode
  :hook (lsp-mode . lsp-ui-mode))

(use-package typescript-mode
  :commands typescript-mode
  :mode "\\.ts$"
  :hook (typescript-mode . lsp-deferred)
  :config
  ; TODO: Can move this to indent.el?
  (setq typescript-indent-level 2))

(use-package ts-comint :commands run-ts)

(use-package flycheck
  :commands (flycheck-mode)
  :custom ((setq-default flycheck-emacs-lisp-load-path 'inherit)))

(defun ian-setup-js ()
  "JS setup for web mode"
  (when (string-equal "js" (file-name-extension buffer-file-name))
    (nvm-use "14.16.1")
    (setq lsp-eslint-server-command '("eslint-language-server" "--stdio"))
    (setq web-mode-enable-auto-quoting nil)
    (setq sort-fold-case t)))

(defun ian-setup-tsx ()
  "TSX setup for web mode"
  (when (string-equal "tsx" (file-name-extension buffer-file-name))
    (setq web-mode-enable-auto-quoting nil)
    (lsp-deferred)))

(defun ian-setup-web-mode ()
  "Run when entering web-mode"
  ;(ian-setup-js)
  (ian-setup-tsx))


(define-derived-mode json-mode
  js-mode "JSON"
  "Major mode for json is just js mode.")

(add-to-list 'auto-mode-alist '("\\.json?$" . json-mode))
(add-to-list 'auto-mode-alist '("\\.jsx?$" . web-mode))
(add-to-list 'auto-mode-alist '("\\.tsx$" . web-mode))
(add-to-list 'auto-mode-alist '("\\.ejs$" . web-mode))
(add-to-list 'auto-mode-alist '("\\.svelte$" . web-mode))
(setq-default web-mode-content-types-alist '(("jsx" . "\\.js[x]?\\'")))

; Better package for jsx, but doesn't play nice with flow
;(use-package rjsx-mode
;  :config
;  :hook (rjsx-mode . ian-setup-web-mode))

(use-package web-mode
  :commands web-mode
  :hook (web-mode . ian-setup-web-mode))

; Support for R
(use-package ess
  :commands (ess-r-mode R)
  :custom ((ess-r-smart-operators nil)
           (ess-use-flymake nil)))
  ;; :hook (ess-r-mode . lsp-deferred))

(defun ian-org-ess-src-block-eval ()
  "evaluate current source block in ess"
  (interactive)
  (let* ((info (org-babel-get-src-block-info))
         (body (nth 1 info))
         (process (ess-get-process
                   (ess-request-a-process
                    (list "Switch to which ESS process? " current-prefix-arg)))))
    (ess-send-string process body)))

(use-package polymode
  :commands (polymode-minor-mode)
  :straight '(polymode :build (:not compile)))

(use-package poly-noweb
  :commands (polymode-minor-mode)
  :after (polymode)
  :straight '(poly-noweb :build (:not compile)))

(use-package poly-markdown
  :commands (polymode-minor-mode)
  :after (polymode))
  :straight '(poly-markdown :build (:not compile))

(use-package poly-R
  :commands (polymode-minor-mode)
  :after (polymode poly-markdown poly-noweb))
  :straight '(poly-R :build (:not compile))

(add-to-list 'auto-mode-alist '("\\.Rmd" . poly-markdown+r-mode))

(use-package haskell-mode
  :commands haskell-mode)

(use-package lsp-haskell
  :commands (haskell-mode)
  :config
  (setq lsp-haskell-server-path "/home/ian/.ghcup/bin/haskell-language-server-wrapper")
  :hook
  (haskell-mode . lsp)
  (haskell-literate-mode . lsp))

(use-package vimrc-mode :commands vimrc-mode)

(add-hook 'js-mode-hook
  (lambda ()
    (setq sort-fold-case t)))

;; (add-hook 'python-mode-hook
;;   (lambda ()
;;     (lsp-deferred)))

(use-package elpy
  :disabled
  :defer
  :init
  (elpy-enable))

(defun ian-editorconfig-start ()
  (let ((root (projectile-project-root)))
    (when (and root (expand-file-name ".editorconfig" root))
      (editorconfig-mode))))

(use-package editorconfig
  ; :commands editorconfig-mode
  :defer
  :diminish
  ;:hook (prog-mode . ian-editorconfig-start)
  )

(add-hook 'ruby-mode-hook
  (lambda ()
    (lsp-deferred)))

(use-package csv-mode
  :commands csv-mode
  :hook (csv-mode . csv-align-mode))

(use-package lua-mode
  :commands lua-mode)

(use-package clojure-mode
  :commands clojure-mode)

(use-package cider
  :commands cider-jack-in)

(use-package nvm
  ;; :custom (nvm-dir "~/.config/nvm")
  :custom (nvm-dir "~/.nvm")
  :commands nvm-use)

(use-package coffee-mode
  :commands coffee-mode)

(use-package racket-mode
  :commands racket-mode)

(use-package geiser
  :commands (geiser-connect run-geiser)
  :custom
  (geiser-active-implementations '(guile)))

(use-package indium
  :commands (indium-connect indium-launch))

(use-package js-comint
  :config
  (js-do-use-nvm))

(use-package groovy-mode :commands groovy-mode)
(use-package kotlin-mode :commands kotlin-mode)

(use-package lispy
  :hook ((emacs-lisp-mode . lispy-mode)
         (scheme-mode . lispy-mode)))

(use-package lispyville
  :hook ((lispy-mode . lispyville-mode))
  :config
  (lispyville-set-key-theme '(operators c-w additional
                              additional-movement slurp/barf-cp
                              prettify)))

(use-package ielm
  :commands (ielm projectile-run-ielm)
  :config
  ; https://github.com/hlissner/doom-emacs/blob/ee890064f853b1a4f61965e155cff0dffc58918e/modules/lang/emacs-lisp/config.el
  ; http://www.modernemacs.com/post/comint-highlighting/
  (setq ielm-font-lock-keywords
        (append '(("\\(^\\*\\*\\*[^*]+\\*\\*\\*\\)\\(.*$\\)"
                   (1 font-lock-comment-face)
                   (2 font-lock-constant-face)))
                (when (require 'highlight-numbers nil t)
                  (highlight-numbers--get-regexp-for-mode 'emacs-lisp-mode))
                (cl-loop for (matcher . match-highlights)
                         in (append lisp-el-font-lock-keywords-2
                                    lisp-cl-font-lock-keywords-2)
                         collect
                         `((lambda (limit)
                             (when ,(if (symbolp matcher)
                                        `(,matcher limit)
                                      `(re-search-forward ,matcher limit t))
                               ;; Only highlight matches after the prompt
                               (> (match-beginning 0) (car comint-last-prompt))
                               ;; Make sure we're not in a comment or string
                               (let ((state (syntax-ppss)))
                                 (not (or (nth 3 state)
                                          (nth 4 state))))))
                           ,@match-highlights)))))

(use-package yaml-mode :commands yaml-mode)

; Dependencies need to be available to package.el for package-lint to work correctly
; (add-to-list 'package-archives '("melpa" . "https://melpa.org/packages/") t)
(use-package package-lint :commands package-lint-current-buffer)

(use-package tree-sitter :commands tree-sitter-mode)

(use-package tree-sitter-langs
  :commands tree-sitter-mode
  :after tree-sitter)

(use-package tree-sitter-indent
  :commands tree-sitter-mode
  :after tree-sitter)

(use-package csharp-mode
  :commands csharp-mode
  :hook (csharp-mode . lsp-deferred)
  :config
  (add-to-list 'auto-mode-alist '("\\.json?$" . json-mode)))

(use-package fennel-mode
  :commands fennel-mode)

(use-package friar 
  :commands friar
  :straight (:host github :repo "warreq/friar" :branch "master"
	     :files (:defaults "*.lua" "*.fnl"))
  :custom
  (friar-fennel-file-path "~/Code/oss/linux/fennel/fennel"))

(use-package go-mode :commands go-mode)

(use-package dart-mode :commands dart-mode
  :hook (dart-mode . lsp-deferred))

(use-package lsp-dart
  :commands dart-mode
  :hook (dart-mode . lsp-deferred))

(use-package ob-dart :defer 0.1)

(use-package rust-mode
  :commands rust-mode
  :config
  (setq lsp-rust-server 'rust-analyzer)
  :hook (dart-mode . lsp-deferred))

(use-package dockerfile-mode
  :commands dockerfile-mode)
