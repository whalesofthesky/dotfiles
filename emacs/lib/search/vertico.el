(use-package selectrum
  :disabled t
  :after general
  :config
  (selectrum-mode 1)
  (general-define-key
   :keymaps '(selectrum-minibuffer-map)
   "<tab>" 'selectrum-next-candidate
   "M-<tab>" 'selectrum-insert-current-candidate
   "M-RET" 'selectrum-insert-current-candidate
   "<backtab>" 'selectrum-previous-candidate))

(use-package selectrum-prescient
  :disabled t
  :after selectrum
  :config
  (prescient-persist-mode 1)
  (selectrum-prescient-mode 1))

(use-package vertico
  :after general
  :config
  (vertico-mode 1)
  (general-define-key
   :keymaps '(vertico-map)
   "M-v" 'switch-to-completions
   "<tab>" 'vertico-next
   "M-<tab>" 'vertico-insert
   "M-RET" 'vertico-insert
   "<backtab>" 'vertico-previous))

(use-package orderless
  :after vertico
  :config
  (setq completion-styles '(orderless)
        completion-category-defaults nil
        completion-category-overrides '((file (styles partial-completion)))
        completion-ignore-case t
        read-file-name-completion-ignore-case t
        read-buffer-completion-ignore-case t
        orderless-smart-case t ; C-h v on this for more info about ^^^
        ))

(use-package savehist
  :after vertico
  :config
  (setq history-length 1000)
  (savehist-mode 1))

; TODO: Move this stuff to the right place
(use-package emacs
  :init
  ;; Add prompt indicator to `completing-read-multiple'.
  ;; Alternatively try `consult-completing-read-multiple'.
  (defun crm-indicator (args)
    (cons (concat "[CRM] " (car args)) (cdr args)))
  (advice-add #'completing-read-multiple :filter-args #'crm-indicator)

  ;; Do not allow the cursor in the minibuffer prompt
  (setq minibuffer-prompt-properties
        '(read-only t cursor-intangible t face minibuffer-prompt))
  (add-hook 'minibuffer-setup-hook #'cursor-intangible-mode)

  ;; Emacs 28: Hide commands in M-x which do not work in the current mode.
  ;; Vertico commands are hidden in normal buffers.
  ;; (setq read-extended-command-predicate
  ;;       #'command-completion-default-include-p)

  ;; Enable recursive minibuffers
  (setq enable-recursive-minibuffers t))

(use-package consult
  :after projectile
  :config
  (setq consult-project-root-function #'projectile-project-root)
  (setq completion-in-region-function #'consult-completion-in-region))

;; Enable richer annotations using the Marginalia package
(use-package marginalia
  :after vertico
  ;; Either bind `marginalia-cycle` globally or only in the minibuffer
  :bind (("M-A" . marginalia-cycle)
         :map minibuffer-local-map
         ("M-A" . marginalia-cycle))

  ;; The :init configuration is always executed (Not lazy!)
  :init

  ;; Must be in the :init section of use-package such that the mode gets
  ;; enabled right away. Note that this forces loading the package.
  (marginalia-mode)

  ;; When using Selectrum, ensure that Selectrum is refreshed when cycling annotations.
  (advice-add #'marginalia-cycle :after
              (lambda () (when (bound-and-true-p selectrum-mode) (selectrum-exhibit 'keep-selected))))

  ;; Prefer richer, more heavy, annotations over the lighter default variant.
  ;; E.g. M-x will show the documentation string additional to the keybinding.
  ;; By default only the keybinding is shown as annotation.
  ;; Note that there is the command `marginalia-cycle' to
  ;; switch between the annotators.
  (setq marginalia-annotators '(marginalia-annotators-heavy marginalia-annotators-light nil))
)

(use-package helpful
  :commands (helpful-callable helpful-command helpful-variable helpful-key)
  :bind
  ([remap describe-function] . helpful-callable)
  ([remap describe-command] . helpful-command)
  ([remap describe-variable] . helpful-variable)
  ([remap describe-key] . helpful-key))
