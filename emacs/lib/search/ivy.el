(use-package ivy
  :diminish
  :bind (
    :map ivy-minibuffer-map
    ("TAB" . ivy-next-line)
    ("<backtab>" . ivy-previous-line))
  :config
  (ivy-mode 1))

(use-package ivy-hydra :after ivy)

(use-package counsel
  :bind (("M-x" . counsel-M-x)
         ("C-x C-f" . counsel-find-file)))

(use-package ivy-rich
  :diminish
  :init
  (ivy-rich-mode 1))

(use-package ivy-prescient
  :after counsel
  :custom
  ; https://github.com/raxod502/prescient.el/issues/92
  (prescient-use-char-folding nil)
  :config
  (prescient-persist-mode 1)
  (ivy-prescient-mode 1))

(use-package helpful
  :custom
  (counsel-describe-function-function #'helpful-callable)
  (counsel-describe-variable-function #'helpful-variable)
  :bind
  ([remap describe-function] . counsel-describe-function)
  ([remap describe-command] . helpful-command)
  ([remap describe-variable] . counsel-describe-variable)
  ([remap describe-key] . helpful-key))

; need this for projectile rg
(use-package counsel-projectile
  :after general
  :custom
  (counsel-projectile-switch-project-action
   '(1 ("d" counsel-projectile-switch-project-action-dired "dired")
       ;; (ian-projectile-new-perspective-action)
       ))
  :config
  (counsel-projectile-mode)
  (general-define-key
   :states '(normal)
   "C-p" 'counsel-projectile-find-file)
  (general-define-key
   :prefix ","
   :states '(normal)
   "b s" 'counsel-projectile-switch-to-buffer
   "f" 'counsel-find-file
   "p r"   'counsel-projectile-rg
   "p s"   'counsel-projectile-switch-project
   "x b"   'persp-counsel-switch-buffer))

