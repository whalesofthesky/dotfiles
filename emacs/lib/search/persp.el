(use-package perspective
  :defer 0.1
  :diminish persp-mode
  :custom
  ((persp-show-modestring nil))
  :config
  (unless persp-mode
    (persp-mode 1))
  (general-define-key
   :states '(normal visual)
   :prefix ","
   "x" '(:ignore t :which-key "perspective")
   "x a" 'persp-add-buffer
   "x b" 'switch-to-buffer
   "x r" 'persp-rename
   "x s" 'persp-switch
   "x n" 'persp-next
   "x k" 'persp-kill-buffer*
   "x p" 'persp-prev))

;(use-package persp-mode
;  :after general
;  :diminish persp-mode
;  :init
;  (general-define-key
;   :states '(normal visual)
;   :prefix ","
;   "x" '(:keymap persp-key-map :which-key "persp-mode" :package persp-mode))
;  (setq persp-auto-resume-time 0)
;  (setq persp-auto-save-opt 0)
;  (setq wg-morph-on nil) ;; switch off animation
;  (setq persp-autokill-buffer-on-remove 'kill-weak)
;  (setq persp-show-modestring nil)
;  (add-hook 'after-init-hook #'persp-mode))

(defun ian-persp-current-name ()
  ""
  (persp-current-name))

(defun ian-persp-switch (name)
  "NAME of perspective to switch to."
  (persp-switch name))

(defun ian-ibuffer ()
  "Open ibuffer and cd to wherever you were when you opened it."
  (interactive)
  (let ((dir default-directory))
    (persp-ibuffer nil)
    (cd dir)))

(defun ian-with-persp (name fun)
  "NAME FUN."
  (when (not (eq (ian-persp-current-name) name)) 
    (ian-persp-switch name))
  (funcall fun))
