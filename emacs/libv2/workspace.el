(use-package perspective
  :defer 0.1
  :diminish persp-mode
  :custom
  (persp-show-modestring nil)
  ;; (persp-mode-prefix-key (kbd "C-c M-p"))  ; pick your own prefix key here
  (persp-suppress-no-prefix-key-warning t)
  :config
  (unless persp-mode
    (persp-mode 1))
  (general-define-key
   :states '(normal visual)
   :prefix ","
   "x" '(:ignore t :which-key "perspective")
   "x a" 'persp-add-buffer
   "x b" 'switch-to-buffer
   "x r" 'persp-rename
   "x s" 'persp-switch
   "x n" 'persp-next
   "x k" 'persp-kill-buffer*
   "x p" 'persp-prev))

(defun ian-persp-current-name ()
  ""
  (persp-current-name))

(defun ian-persp-switch (name)
  "NAME of perspective to switch to."
  (persp-switch name))

(defun ian-ibuffer ()
  "Open ibuffer and cd to wherever you were when you opened it."
  (interactive)
  (let ((dir default-directory))
    (persp-ibuffer nil)
    (cd dir)))

(defun ian-with-persp (name fun)
  "NAME FUN."
  (when (not (eq (ian-persp-current-name) name)) 
    (ian-persp-switch name))
  (funcall fun))
