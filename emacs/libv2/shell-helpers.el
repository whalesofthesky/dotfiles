(defun ian-async-shell-command (command)
  ""
  (interactive "sCommand: ")
  (let* ((buffer-name (concat "*Async Command<" command ">*"))
         (buffer-process (get-buffer-process buffer-name)))
    (if (not buffer-process)
        (async-shell-command command buffer-name)
      (when (y-or-n-p "Process already running. Restart? ")
        (ian-async-shell-command--on-process-killed
         buffer-process
         `(lambda () (async-shell-command ,command ,buffer-name)))
        (kill-process buffer-process)))))

(defun ian-async-shell-command--buffer-select ()
  ""
  (completing-read
   "Buffer to kill: "
   (seq-filter (lambda (s) (s-starts-with-p "*Async" s))
               (seq-map #'buffer-name (buffer-list)))))

(defun ian-async-shell-command--on-process-killed (process callback)
  ""
  (set-process-sentinel
   process
   (lambda (process status)
      (when (equal status "killed\n")
        (callback)))))
  
(defun ian-async-shell-command-switch-to-buffer ()
  ""
  (interactive)
  (let* ((name (ian-async-shell-command--buffer-select))
         (buffer (get-buffer name)))
    (switch-to-buffer buffer)))

(defun ian-async-shell-command-switch-to-buffer-other-window ()
  ""
  (interactive)
  (let* ((name (ian-async-shell-command--buffer-select))
         (buffer (get-buffer name)))
    (switch-to-buffer-other-window buffer)))

(defun ian-async-shell-command-kill ()
  ""
  (interactive)
  (let* ((name (completing-read
                "Buffer to kill: "
                (seq-filter (lambda (s) (s-starts-with-p "*Async" s))
                            (seq-map #'buffer-name (buffer-list)))))
         (buffer (get-buffer name))
         (buffer-process (get-buffer-process buffer)))
    (if (not buffer-process)
        (kill-buffer buffer)
      (ian-async-shell-command--on-process-killed
       buffer-process
       `(lambda () (kill-buffer ,buffer)))
      (kill-process buffer-process))))
