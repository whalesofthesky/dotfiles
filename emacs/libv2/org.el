(setq org-link-frame-setup 
      '((vm . vm-visit-folder-other-frame)
       (vm-imap . vm-visit-imap-folder-other-frame)
       (gnus . org-gnus-no-new-news)
       (file . find-file)
       (wl . wl-other-frame)))

(setq org-todo-keywords
      '((sequence "TODO(t)" "NEXT(n)" "IN PROGRESS(i)" "|" "DONE(d!)" "WONTDO(w@)")
	(sequence "SHOW(s)" "|" "HIDE(h!)")
	(sequence "PROJECT(p)" "|" "PROJECT-DONE(D!)")))

;; Org Mode
(use-package org-roam
  :init
  (setq org-roam-database-connector 'sqlite-builtin)
  (setq org-roam-directory (ian-resolve-file-name "~/Documents/org/org-roam"))
  :config
  (org-roam-db-autosync-mode))

;; org funcs
(defun ian-org-get-template-header ()
  "Copy template header."
  (interactive)
  (save-excursion
    (beginning-of-buffer)
    (search-forward-regexp "^\* Template$")

    (org-mark-subtree)
    ;; Deselect the heading line
    (next-line 1)
    ;; Copy the current region
    (kill-ring-save
     (region-beginning)
     (region-end))
    ;; Deselect the region after copying
    (deactivate-mark)
    (substring (pop kill-ring) 0 -1)))

(defun ian-org-get-date-heading-text ()
  "Create text for date heading."
  (format-time-string "* %Y-%m-%d" (current-time)))

(defun ian-org-create-date-heading ()
  "Create a heading at the top of the file with todays date."
  (interactive)
  (save-excursion
    (beginning-of-buffer)
    (end-of-line 0)
    (open-line 1)
    (insert (ian-org-get-date-heading-text)
	    "\n"
	    (ian-org-get-template-header))))


