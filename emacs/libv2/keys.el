(global-set-key (kbd "<escape>") 'keyboard-escape-quit)

(use-package evil
  :defer 1
  :init
  ; see evil-collection
  (setq evil-want-keybinding nil)
  :custom
  (evil-undo-system 'undo-redo)
  :config
  (define-key evil-insert-state-map (kbd "C-g") 'evil-normal-state)
  (define-key evil-motion-state-map (kbd ",") nil)
  (define-key evil-motion-state-map (kbd "SPC") nil)
  ;(define-key org-agenda-keymap (kbd ",") nil)
  ;(define-key org-agenda-keymap (kbd "SPC") nil)
  (evil-mode t))

(use-package evil-collection :after evil :config (evil-collection-init))

(use-package evil-easymotion :after evil)

(use-package evil-surround
  :after evil
  :config
  (global-evil-surround-mode 1))

(use-package evil-org
  :commands org-mode
  :hook (org-agenda-mode . evil-org-mode)
  :config
  (evil-org-set-key-theme '(navigation insert textobjects additional calendar))
  (require 'evil-org-agenda)
  (evil-org-agenda-set-keys))

(use-package general
  :after (evil evil-easymotion)
  :config
  (general-define-key
   :states '(normal visual motion)
   :prefix "SPC"
   ;; "SPC"   '(:ignore t :which-key "easymotion")
   "a"     '(:ignore t :which-key "ace-window")
   "a a"   'ace-window
   "a s"   'ace-swap-window
   "b"     '(:ignore t :which-key "buffers")
   "b c"   'insert-char
   "b e"   'ian-ibuffer
   "b n"   'switch-to-next-buffer
   "b p"   'switch-to-prev-buffer
   "b s"   'projectile-switch-to-buffer
   "c"     '(:ignore t :which-key "comments")
   "c c"   'comment-region
   "c u"   'uncomment-region
   "e"     '(:ignore t :which-key "flycheck errors")
   "e h"   'ian-hydra-flycheck/body
   "e n"   'flycheck-next-error
   "e p"   'flycheck-previous-error
   "f"     'find-file
   "g"     '(:ignore t :which-key "git")
   "g b"   'magit-blame
   "g d"   'magit-diff
   "g h"   'ian-hydra-git/body
   "g s"   'magit-status
   "h s"   'ian-split-window-below
   "i"     'ian-edit-init
   "k b"   'ian-kill-buffer-name
   "l"     '(:keymap lsp-command-map :which-key "lsp" :package lsp-mode)
   "m"     '(:ignore t :which-key "manager")
   "m c"   'ian-calendar
   "m e"   'ian-manager-exercise
   "m q"   'ian-wikipedia
   "m t"   'ian-manager-today
   "m w"   'ian-manager-weekly
   "n"     '(:ignore t :which-key "'nerdtree'")
   "n f"   'ian-dired-sidebar-find
   "n t"   'ian-dired-sidebar-root
   "o"     '(:ignore t :which-key "org-mode")
   "o a"   'org-agenda
   "o b r" 'ian-org-ess-src-block-eval
   "o c"   'org-roam-capture
   "o d"   'org-deadline
   "o e"   'org-babel-execute-src-block
   "o f"   'org-roam-node-find
   "o i"   'org-insert-structure-template
   "o l"   'org-latex-preview
   "o m"   'org-toggle-inline-images
   "o n"   'org-babel-next-src-block
   "o o"   'org-open-at-point
   "o p"   'org-babel-previous-src-block
   "o r"   'org-refile
   "o s"   'org-schedule
   "o t"   'org-todo
   "o d"   'org-roam-dailies-goto-today
   "o y"   'org-roam-dailies-goto-yesterday
   ; "po" 'projectile-switch-open-project
   "p"     '(:ignore t :which-key "projectile")
   "p d"   'projectile-dired
   "p e"   'projectile-run-eshell
   "p f"   'projectile-find-file
   "p r"   'consult-ripgrep
   "p s"   'projectile-switch-project
   "p v"   'projectile-run-vterm
   "r"     '(:ignore t :which-key "R")
   "r c"   'polymode-eval-region-or-chunk
   "r l"   'ess-eval-line
   "r n"   'polymode-next-chunk
   "r p"   'polymode-previous-chunk
   "r r"   'ess-eval-region
   "r x"   'polymode-export
   "s"     '(:ignore t :which-key "strings")
   "s l"   'sort-lines
   "s c"   'string-inflection-lower-camelcase
   "s k"   'string-inflection-kebab-case
   "s u"   'string-inflection-underscore
   "t"     '(:ignore t :which-key "text")
   "t s"   'hydra-text-scale/body
   "v s"   'ian-split-window-right
   "v v"   'vterm
   "x"     '(:ignore t :which-key "perspective")
   "x a"   'persp-add-buffer
   "x b"   'switch-to-buffer
   "x r"   'persp-rename
   "x s"   'persp-switch
   "x n"   'persp-next
   "x k"   'persp-kill-buffer*
   "x p"   'persp-prev
   "x v"   'ian-persp-vterm
   "w"     'save-buffer
   "q"     'evil-quit)
  ; Unset some evil mode key bindings that I don't need
  (general-define-key
   :states 'normal
   "C-n" nil
   "C-p" nil)
  (general-define-key
   :states 'normal
   :keymaps '(comint-mode-map)
   "C-p" nil)
  ; might get rid of this one eventually...
  (general-define-key
   :states '(normal visual)
   "C-p" 'projectile-find-file)
  ; evil-easymotion
  (general-define-key
   :states 'normal
   "SPC SPC" '(:def evilem-map :keymap evil-motion-state-map))
  (general-define-key
   :states 'visual
   "SPC SPC" '(:def evilem-map :keymap evil-motion-state-map))
  ; turn off ess "smart comma" keybinding and ibuffer "sort" keybinding
  (general-define-key
   :keymaps '(
              elfeed-search-mode-map
              elfeed-show-mode-map
              ess-mode-map
              ess-r-mode-map
              help-mode-map
              inferior-ess-mode-map
              inferior-ess-r-mode-map
              magit-mode-map
              org-agenda-mode-map
              )
   "SPC" nil)
  (general-define-key
   :states '(normal motion visual)
   :keymaps '(
              elfeed-search-mode-map
              elfeed-show-mode-map
              ess-mode-map
              ess-r-mode-map
              help-mode-map
              image-mode-map
              inferior-ess-mode-map
              inferior-ess-r-mode-map
              ibuffer-mode-map
              org-agenda-mode-map
              dired-mode-map
              magit-mode-map
              doc-view-mode-map)
   "SPC" nil)
  (general-define-key
   :keymaps '(racket-repl-mode-map)
   "C-w" nil)
  ;; place sort on another key, because I still want it, just not on my leader key
  ;; conflicts with evil-collection stuff, disable for now
  ;; (general-define-key
  ;;  :states 'normal
  ;;  :keymaps 'ibuffer-mode-map
  ;;  "s" 'ibuffer-toggle-sorting-mode)
  (general-define-key
   :states 'normal
   :keymaps 'org-mode-map
   "RET" 'org-open-at-point)
  (general-define-key
   :states '(insert)
   :keymaps 'org-mode-map
   "M-l" 'org-demote-subtree
   "M-h" 'org-promote-subtree)
  ; FIXME: S-RET keybind appears to do nothing, just regular RET
  (general-define-key
   :states 'normal
   :keymaps 'Info-mode-map
   "RET" 'Info-follow-nearest-node)
  (general-define-key
   :keymaps 'ivy-minibuffer-map
   "S-RET" 'ivy-immediate-done)
  ;; (general-define-key
  ;;  :states '(normal visual)
  ;;  :keymaps 'web-mode-map
  ;;  "SPC" 'ian-hydra-web-mode/body)
  ; TODO: this needs work
  (general-define-key
   :states '(normal)
   "g p" 'ian-select-last-paste)
  (general-define-key
   :states '(normal visual)
   :keymaps '(lsp-mode-map)
   "g r" 'lsp-find-references)
  (general-define-key
   :states '(normal)
   "g /" 'ian-search-current)
  (general-define-key
   :states '(visual)
   "g /" 'ian-search-selected))
