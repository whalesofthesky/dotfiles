" Vimrc Fold Settings {
"  vim: set foldmarker={,} foldlevel=0 foldmethod=marker spell:
" }
" General {
if !exists("g:os")
  if has("win64") || has("win32") || has("win16")
    let g:os = "Windows"
  else
    let g:os = substitute(system('uname'), '\n', '', '')
  endif
endif

" TODO: Confirm runtimepath not needed on windows

" TODO: Confirm this works on windows/osx
set clipboard=unnamedplus
set mouse=a " Enable mouse in all modes
set virtualedit=onemore " allow for cursor beyond last character
" }
" Backup and Persistent Undo {
set backup

if has('persistent_undo')
  set undofile "so is persistent undo ...
  set undolevels=1000 "maximum number of changes that can be undone
  set undoreload=10000 "maximum number lines to save for undo on a buffer reload
endif

" fns must start with capital letter https://learnvimscriptthehardway.stevelosh.com/chapters/23.html
function! IanMkdirP(directory)
  if exists("*mkdir")
    if !isdirectory(a:directory)
      call mkdir(a:directory)
    endif
  endif
endfunction

function! IanInitDirs()
  let separator = "."
  let parent = $XDG_CONFIG_HOME
  let prefix = 'nvim/data'
  let dir_list = {
        \ 'backup': 'backupdir',
        \ 'views': 'viewdir',
        \ 'swap': 'directory' }

  if has('persistent_undo')
    let dir_list['undo'] = 'undodir'
  endif

  let vim_directory = parent . '/' . prefix . '/'

  call IanMkdirP(vim_directory)

  for [dirname, settingname] in items(dir_list)
    let directory = parent . '/' . prefix . '/' . dirname . '/'

    call IanMkdirP(directory)

    if !isdirectory(directory)
      echo "Warning: Unable to create backup directory: " . directory
      echo "Try: mkdir -p " . directory
    else
      let directory = substitute(directory, " ", "\\\\ ", "g")
      exec "set " . settingname . "=" . directory
    endif
  endfor
endfunction

call IanInitDirs()
" }
" Search {
set ignorecase " ignore case when searching...
set smartcase  " unless search includes uppercase letters
" }
" Formatting {
set nowrap        " don't wrap long lines
set shiftwidth=2  " use indents of 2 spaces
set shiftround    " shift to a useful indentation
set expandtab     " tabs are spaces, not tabs
set tabstop=2     " set tab size
set softtabstop=2 " let backspace delete indent
set smartindent
" }
" Vim-Plug Setup {
function! IanVimPlugBootstrap()
  " https://github.com/junegunn/vim-plug/wiki/tips
  let data_dir = has('nvim') ? stdpath('data') . '/site' : '~/.vim'
  if empty(glob(data_dir . '/autoload/plug.vim'))
    silent execute '!curl -fLo '.data_dir.'/autoload/plug.vim --create-dirs  https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim'
    autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
  endif

  autocmd VimEnter *
        \  if len(filter(values(g:plugs), '!isdirectory(v:val.dir)'))
        \|   PlugInstall --sync | q
        \| endif
endfunction

call IanVimPlugBootstrap()

let g:ian_plug_dir = $XDG_CONFIG_HOME . "/nvim/bundle"

call plug#begin(g:ian_plug_dir)

Plug 'morhetz/gruvbox' " Color theme
Plug 'embark-theme/vim', { 'as': 'embark', 'branch': 'main' }
Plug 'neovim/nvim-lspconfig' " LSP
Plug 'kien/ctrlp.vim' " Fuzzy file search
Plug 'jlanzarotta/bufexplorer'
Plug 'vim-airline/vim-airline'
Plug 'bakpakin/fennel.vim'
Plug 'akinsho/flutter-tools.nvim'
Plug 'tpope/vim-fugitive'
Plug 'scrooloose/nerdtree'

call plug#end()
" }
" UI {
set termguicolors
color gruvbox

set number " turn on line numbers
set relativenumber " relative line numbers
autocmd InsertEnter * set norelativenumber
autocmd InsertLeave * set relativenumber

set showmatch " show matching brackets/parenthesis
set scrolloff=8 " minimum lines to keep above and below cursor
set list
set listchars=tab:\ \ ,trail:.,extends:#,nbsp:. " Highlight problematic whitespace

function! IanAutoCd()
  if filereadable(@%)
    execute('cd ' . fnamemodify(resolve(@%), ':h'))
  endif
endfunction

" autocmd BufEnter * call IanAutoCd()
" }
" General Keybindings {
let mapleader=' ' " Set leader key

set wildcharm=<TAB> " Trigger wildmenu from key bindings with TAB

" Essential Quality of Life 
nnoremap <Leader>nh <cmd>noh<CR>
nnoremap <Leader>w <cmd>w<CR>
nnoremap <Leader>q <cmd>q<CR>
nnoremap <Leader>i <cmd>execute('e ' . resolve($MYVIMRC))<CR>

" Buffers
nnoremap <Leader>bd :bdelete<CR>
nnoremap <Leader>bn :bnext<CR>
nnoremap <Leader>bp :bprevious<CR>

nnoremap <Leader>bs :buffer <TAB>

" Sorting and string inflection (TODO)
vnoremap <Leader>sl :sort i<CR>

" Move lines with + and minus
nnoremap - ddkP
nnoremap + ddp
vnoremap - dkP`[V`]
vnoremap + dp`[V`]

" replace selected text with default register without yanking it
vnoremap <Leader>p "_dP

tnoremap <Esc> <C-\><C-n>

" inoremap <M-Backspace> <Esc>vBdi
" }
" NERDTree {
let NERDTreeShowHidden=1
let NERDTreeIgnore=['\.pyc$']
nmap <Leader>nt :NERDTreeToggle<CR>
nmap <Leader>nf :NERDTreeFind<CR>
" }
" CtrlP {
nmap <Leader><c-p> :CtrlPClearCache<CR>
nmap <Leader>pf :CtrlP<CR>
let g:ctrlp_custom_ignore = {
      \ 'dir': 'node_modules$'
      \ }
let g:ctrlp_reuse_window = '[BufExplorer]'
" }
" BufExplorer {
let g:bufExplorerShowRelativePath=1
"let g:bufExplorerDisableDefaultKeyMapping=1
nmap <Leader>be :BufExplorer<CR>
nmap <Leader>vs :BufExplorerVerticalSplit<CR>
nmap <Leader>hs :BufExplorerHorizontalSplit<CR>
" }
" Airline {
if has ("unix") && "Darwin" != system("echo -n \"$(uname)\"")
  let g:airline_powerline_fonts = 1
  let g:airline_symbols_ascii = 1
endif
" }
" Fugitive {
nnoremap <leader>gs :Git<CR>
nnoremap <leader>gd :Git diff<CR>
nnoremap <leader>gc :Git commit -a<CR>
nnoremap <leader>gb :Git blame<CR>
nnoremap <leader>gl :Git log -n 7<CR>
nnoremap <leader>gp :Git push<CR>
nnoremap <leader>ga :Git add %<CR>
" }
" LSP {
lua << EOF
-- Typescript
require('lspconfig').tsserver.setup {}

-- Lua
-- https://www.chrisatmachine.com/Neovim/28-neovim-lua-development/
require('lspconfig').sumneko_lua.setup {
  settings = {
    Lua = {
      runtime = {
        -- Tell the language server which version of Lua you're using (most likely LuaJIT in the case of Neovim)
        version = 'LuaJIT',
        -- Setup your lua path
        path = vim.split(package.path, ';')
        },
      diagnostics = {
        -- Get the language server to recognize the `vim` global
        globals = {'vim'}
        },
      workspace = {
        -- Make the server aware of Neovim runtime files
        library = {[vim.fn.expand('$VIMRUNTIME/lua')] = true, [vim.fn.expand('$VIMRUNTIME/lua/vim/lsp')] = true}
        }
      }
    }
  }
EOF

" https://www.chrisatmachine.com/Neovim/27-native-lsp/
nnoremap <silent> gd <cmd>lua vim.lsp.buf.definition()<CR>
nnoremap <silent> gD <cmd>lua vim.lsp.buf.declaration()<CR>
nnoremap <silent> gr <cmd>lua vim.lsp.buf.references()<CR>
nnoremap <silent> gi <cmd>lua vim.lsp.buf.implementation()<CR>
nnoremap <silent> gh <cmd>lua vim.lsp.buf.hover()<CR>
nnoremap <silent> <C-k> <cmd>lua vim.lsp.buf.signature_help()<CR>
nnoremap <silent> en <cmd>lua vim.lsp.diagnostic.goto_prev()<CR>
nnoremap <silent> ep <cmd>lua vim.lsp.diagnostic.goto_next()<CR>
" }
" Help {
function! IanHelpSetup()
  if &filetype != 'help'
    return
  endif

  set buflisted
endfunction

autocmd BufEnter *.txt call IanHelpSetup()
" }
