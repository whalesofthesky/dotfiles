local awful = require("awful")
local gears = require("gears")
local hotkeys_popup = require("awful.hotkeys_popup")
local volume = require("lib.volume")

-- Enable hotkeys help widget for VIM and other apps
-- when client with a matching name is opened:
require("awful.hotkeys_popup.keys")

-- Make this global to awesome client
tag = require("lib.tag")

globalkeys = gears.table.join(
   awful.key({ modkey,           }, "s",      hotkeys_popup.show_help,
      {description="show help", group="awesome"}),
   awful.key({ modkey,           }, "Left",   awful.tag.viewprev,
      {description = "view previous", group = "tag"}),
   awful.key({ modkey,           }, "Right",  awful.tag.viewnext,
      {description = "view next", group = "tag"}),
   awful.key({ modkey,           }, "Escape", awful.tag.history.restore,
      {description = "go back", group = "tag"}),

   awful.key({ modkey,           }, "j",
      function ()
         awful.client.focus.byidx( 1)
      end,
      {description = "focus next by index", group = "client"}
   ),
   awful.key({ modkey,           }, "k",
      function ()
         awful.client.focus.byidx(-1)
      end,
      {description = "focus previous by index", group = "client"}
   ),

   -- Layout manipulation
   awful.key({ modkey, "Shift"   }, "j", function () awful.client.swap.byidx(  1)    end,
      {description = "swap with next client by index", group = "client"}),
   awful.key({ modkey, "Shift"   }, "k", function () awful.client.swap.byidx( -1)    end,
      {description = "swap with previous client by index", group = "client"}),
   awful.key({ modkey,           }, "e", function () awful.screen.focus_relative( 1) end,
      {description = "focus the next screen", group = "screen"}),
   awful.key({ modkey,           }, "w", function () awful.screen.focus_relative(-1) end,
      {description = "focus the previous screen", group = "screen"}),
   awful.key({ modkey, "Shift" }, "e",
      function ()
         local screen = awful.screen.focused()
         local selected_tag = screen.selected_tag
         
         awful.screen.focus_relative(1)

         local next_screen = awful.screen.focused()
         local next_tag = next_screen.selected_tag

         if selected_tag then
            sharedtags.movetag(selected_tag, next_screen)
            sharedtags.viewonly(selected_tag, next_screen)
         end

         if next_tag then
            sharedtags.movetag(next_tag, screen)
            sharedtags.viewonly(next_tag, screen)
         end

         awful.screen.focus_relative(-1)
      end,
      {description = "focus the next screen", group = "screen"}),
   awful.key({ modkey, "Shift" }, "w", 
      function ()
         local screen = awful.screen.focused()
         local selected_tag = screen.selected_tag
         
         awful.screen.focus_relative(-1)

         local next_screen = awful.screen.focused()
         local next_tag = next_screen.selected_tag
         
         if selected_tag then
            sharedtags.movetag(selected_tag, next_screen)
            sharedtags.viewonly(selected_tag, next_screen)
         end

         if next_tag then
            sharedtags.movetag(next_tag, screen)
            sharedtags.viewonly(next_tag, screen)
         end

         awful.screen.focus_relative(1)
      end,
      {description = "focus the previous screen", group = "screen"}),
   awful.key({ modkey,           }, "u", awful.client.urgent.jumpto,
      {description = "jump to urgent client", group = "client"}),
   awful.key({ modkey,           }, "Tab",
      function ()
         awful.client.focus.history.previous()
         if client.focus then
            client.focus:raise()
         end
      end,
      {description = "go back", group = "client"}),

   -- Standard program
   awful.key({ modkey, "Shift" }, "Return", function () awful.spawn(terminal) end,
      {description = "open a terminal", group = "launcher"}),
   awful.key({ modkey, "Shift" }, "x", function () awful.spawn("emacsclient -c") end,
      {description = "start emacsclient", group = "launcher"}),
   awful.key({ modkey, "Shift" }, "r",
      function ()
         os.execute("~/Code/system/dotfiles/bin/setup-xrandr.sh")
         os.execute("xmodmap ~/.xmodmap")
         awesome.restart()
      end,
      {description = "reload awesome", group = "awesome"}),
   awful.key({ modkey, "Shift"   }, "q", awesome.quit,
      {description = "quit awesome", group = "awesome"}),

   awful.key({ modkey,           }, "l",     function () awful.tag.incmwfact( 0.05)          end,
      {description = "increase master width factor", group = "layout"}),
   awful.key({ modkey,           }, "h",     function () awful.tag.incmwfact(-0.05)          end,
      {description = "decrease master width factor", group = "layout"}),
   awful.key({ modkey, "Shift"   }, "h",     function () awful.tag.incnmaster( 1, nil, true) end,
      {description = "increase the number of master clients", group = "layout"}),
   awful.key({ modkey, "Shift"   }, "l",     function () awful.tag.incnmaster(-1, nil, true) end,
      {description = "decrease the number of master clients", group = "layout"}),
   awful.key({ modkey, "Control" }, "h",     function () awful.tag.incncol( 1, nil, true)    end,
      {description = "increase the number of columns", group = "layout"}),
   awful.key({ modkey, "Control" }, "l",     function () awful.tag.incncol(-1, nil, true)    end,
      {description = "decrease the number of columns", group = "layout"}),
   awful.key({ modkey,           }, "space", function () awful.layout.inc( 1)                end,
      {description = "select next", group = "layout"}),
   awful.key({ modkey, "Shift"   }, "space", function () awful.layout.inc(-1)                end,
      {description = "select previous", group = "layout"}),

   awful.key({ modkey, "Control" }, "n",
      function ()
         local c = awful.client.restore()
         -- Focus restored client
         if c then
            c:emit_signal(
               "request::activate", "key.unminimize", {raise = true}
            )
         end
      end,
      {description = "restore minimized", group = "client"}),

   -- Prompt
   awful.key({ modkey },            "r",
      function () awful.screen.focused():emit_signal("ian::promptbox-run") end,
      {description = "run prompt", group = "launcher"}),

   awful.key({ modkey }, "x",
      function () awful.screen.focused():emit_signal("ian::promptbox-execute") end,
      {description = "lua execute prompt", group = "awesome"}),
   awful.key({ modkey }, "p", function() awful.spawn("rofi -show drun") end,
      {description = "launch apps", group = "launcher"}),
   awful.key({ modkey, "Shift" }, "p",
      function()
         awful.spawn.with_shell(
            "echo -e \"" ..
            client_names_rofi() ..
            "\" | rofi -dmenu -i -p \"Tags\" | xargs -I % awesome-client \"tag.add_client_to_tag_by_name('%')\""
         )
      end,
      {description = "select new tag for client", group = "launcher"}),
   awful.key({ modkey }, "o",
      function()
         awful.spawn.with_shell(
            "echo -e \"" ..
            tag.tag_names_rofi() ..
            "\" | rofi -dmenu -i -p \"Tags\" | xargs -I % awesome-client \"tag.view_tag_by_name('%')\""
         )
      end,
      {description = "switch tags", group = "launcher"}),
   awful.key({ modkey, "Shift" }, "o",
      function()
         awful.spawn.with_shell(
            "echo -e \"" ..
            tag.tag_names_rofi() ..
            "\" | rofi -dmenu -i -p \"Tags\" | xargs -I % awesome-client \"tag.send_client_to_tag_by_name('%')\""
         )
      end,
      {description = "select new tag for client", group = "launcher"}),
   awful.key({ modkey }, "b",
      function()
         local bar = awful.screen.focused().mywibox
         bar.visible = not bar.visible
      end,
      {description = "toggle bar visibility", group = "ui"}),
   awful.key({ modkey, "Shift", }, "b",
      function()
         awful.spawn("manager goodphones")
      end,
      {description = "toggle bar visibility", group = "ui"}),
   awful.key({ modkey }, "g",
      function()
         local screen = awful.screen.focused()
         local selected_tag = screen.selected_tag

         if not selected_tag then return end

         if selected_tag.gap == 0 then
            selected_tag.gap = beautiful.useless_gap
         else
            selected_tag.gap = 0
         end
      end,
      {description = "toggle gaps", group = "ui"}),
   awful.key({}, "XF86AudioRaiseVolume", volume.raise_volume),
   awful.key({}, "XF86AudioLowerVolume", volume.lower_volume),
   awful.key({}, "XF86AudioMute", volume.mute_volume) 
)

-- Bind all key numbers to tags.
-- Be careful: we use keycodes to make it work on any keyboard layout.
-- This should map on the top row of your keyboard, usually 1 to 9.
for i = 1, 9 do
   globalkeys = gears.table.join(
      globalkeys,
      -- View tag only.
      awful.key({ modkey }, "#" .. i + 9,
         function ()
            local t = tag.get_tag(i)

            if t then
               tag.view_only(t)
            end
         end,
         {description = "view tag #"..i, group = "tag"}),
      -- Toggle tag display.
      awful.key({ modkey, "Control" }, "#" .. i + 9,
         function ()
            local t = tag.get_tag(i)
            if t then
               tag.view_only(t)
            end
         end,
         {description = "toggle tag #" .. i, group = "tag"}),
      -- Move client to tag.
      awful.key({ modkey, "Shift" }, "#" .. i + 9,
         function ()
            if client.focus then
               local t = tag.get_tag(i)

               if t then
                  client.focus:move_to_tag(t)
               end
            end
         end,
         {description = "move focused client to tag #"..i, group = "tag"}),
      -- Toggle tag on focused client.
      awful.key({ modkey, "Control", "Shift" }, "#" .. i + 9,
         function ()
            if client.focus then
               local t = tag.get_tag(i)

               if t then
                  client.focus:toggle_tag(t)
               end
            end
         end,
         {description = "toggle focused client on tag #" .. i, group = "tag"})
   )
end

root.keys(globalkeys)
