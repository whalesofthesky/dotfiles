-- If LuaRocks is installed, make sure that packages installed through it are
-- found (e.g. lgi). If LuaRocks is not installed, do nothing.
pcall(require, "luarocks.loader")

package.path = package.path .. ";./?/?.lua;/mnt/home/ian/Code/oss/linux/fennel/?.lua;/mnt/home/ian/Code/oss/linux/?/init.lua"
local fennel = require("fennel")
fennel.path = fennel.path .. ";/mnt/home/ian/Code/system/dotfiles/awesome/?.fnl"
table.insert(package.loaders or package.searchers, fennel.searcher)

gears = require("gears")
-- Standard awesome library

awful = require("awful")
-- Widget and layout library
wibox = require("wibox")
-- Theme handling library
beautiful = require("beautiful")
-- Notification library
naughty = require("naughty")

require("awful.autofocus")

sharedtags = require("awesome-sharedtags")

-- NOTE: /usr/share/awesome/themes/default/theme.lua
-- beautiful.init(gears.filesystem.get_themes_dir() .. "default/theme.lua")
beautiful.init(".config/awesome/lib/theme.lua")

require("lib.var")
require("lib.startup")
require("lib.layout")

require("lib.client")
require("lib.global_keys")
-- require("lib.client-new")
require("lib.root_mouse")
require("lib.screen")
