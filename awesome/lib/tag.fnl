(local awful (require "awful"))
(local sharedtags (require "awesome-sharedtags"))

(local use-shared-tags true)

(var tags nil)

(local tags-def [
   {:name "emacs" :layout (. awful.layout.layouts 1)}
   {:name "firefox" :layout (. awful.layout.layouts 1)}
   {:name "chrome" :layout (. awful.layout.layouts 1)}
   {:name "spotify" :layout (. awful.layout.layouts 1)}
   {:name "IDE" :layout (. awful.layout.layouts 1)}
   {:name "games" :layout (. awful.layout.layouts 1)}
   {:name "misc-1" :layout (. awful.layout.layouts 1)}
   {:name "misc-2" :layout (. awful.layout.layouts 1)}
   {:name "misc-3" :layout (. awful.layout.layouts 1)}])

(when use-shared-tags
   (set tags (sharedtags tags-def)))

(fn tag-names-rofi []
  (var name-string "")
  (var count 0)
  (each [i tag-def (pairs tags-def)]
    (set count (+ count 1))
    (local tag (if use-shared-tags
                   (. tags tag-def.name)
                   (. (awful.screen.focused) :tags i)))
    (when (> (string.len name-string) 0)
      (set name-string (.. name-string "\n")))
    (set name-string (.. name-string tag.name))
    (var client-string "")
    (var clients (tag:clients))
    (each [i c (pairs clients)]
      (when (> (string.len client-string) 0)
        (set client-string (.. client-string ",")))

      (set client-string (.. client-string (or c.class c.name))))

      (when (> (string.len client-string) 0)
        (set name-string (.. name-string " (" client-string ")"))))
  name-string)

(fn get-tag [i]
   (if use-shared-tags
      (. tags i)
      (. (awful.screen.focused) :screen :tags i)))

(fn view-only [tag]
  (if use-shared-tags
      (let [screen (awful.screen.focused)
            previous-tag screen.selected_tag]
        (when (and tag.selected previous-tag (~= tag.screen screen))
          (sharedtags.viewonly previous-tag tag.screen))
        (sharedtags.viewonly tag screen))
      (tag:view_only)))

(fn split-tag-name [tag-name-plus]
  ;; Because of the primitive piping/dmenu command that we're
  ;; using with rofi, tag names have a bunch of client info added
  ;; to the end of the string that we may need to strip.
  (if (string.find tag-name-plus "[%s\n].*")
      (string.gsub tag-name-plus "[%s\n].*" "")
      tag-name-plus))

(fn view-tag-by-name [tag-name-plus]
   (let [tag-name (split-tag-name tag-name-plus)]
     (each [i tag (pairs tags)]
       (when (= tag.name tag-name)
         (view-only tag)))))

(fn send-client-to-tag-by-name [tag-name-plus]
  (let [tag-name (split-tag-name tag-name-plus)]
    (each [i t (pairs tags)]
      (when (= t.name tag-name)
        (client.focus:move_to_tag t)
        (awful.screen.focus t.screen)
        (lua "return")))))

(fn create-default-tags [s]
   (awful.tag
      [ "1" "2" "3" "4" "5" "6" "7" "8" "9" ]
      s
      (. awful.layout.layouts 1)))

; Legacy code expects underscores
{
 :use_shared_tags use-shared-tags
 :use-shared-tags use-shared-tags
 :tag_names_rofi tag-names-rofi
 :tag-names-rofi tag-names-rofi
 :get_tag get-tag
 :get-tag get-tag
 :view_only view-only
 :view-only view-only
 :view_tag_by_name view-tag-by-name
 :view-tag-by-name view-tag-by-name
 :send_client_to_tag_by_name send-client-to-tag-by-name
 :send-client-to-tag-by-name send-client-to-tag-by-name
 :create_default_tags create-default-tags
 :create-default-tags create-default-tags
 :split_tag_name split-tag-name
 :split-tag-name split-tag-name
}
