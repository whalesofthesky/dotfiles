(local awful (require :awful))
(local gears (require :gears))
(local hotkeys_popup (require :awful.hotkeys_popup))
(local naughty (require :naughty))
(local bluetooth (require :lib.bluetooth))
(local volume (require :lib.volume))

(local config (require :lib.var))
(local modkey config.modkey)

(local rofi (require :lib.rofi))
(local tag (require :lib.tag))

(local global-keys {})

(fn define-global-key [mods key on-press data]
  "Define a global key binding"
  (gears.table.merge
   global-keys
   (awful.key mods key on-press data))
  (root.keys global-keys))

;; Help
(define-global-key [modkey] "s"
                   hotkeys_popup.show_help
                   {:description "show help" :group "awesome"})

                                        ; Awesome
(define-global-key [modkey "Shift"] "q"
                   awesome.quit
                   {:description "quit awesome" :group "awesome"})

(define-global-key [modkey "Shift"] "r"
                   (fn []
                     (os.execute "~/Code/system/dotfiles/bin/setup-xrandr.sh")
                     ;; (os.execute "xmodmap ~/.xmodmap")
                     (awesome.restart))
                   {:description "reload awesome" :group "awesome"})

                                        ; Rofi
(define-global-key [modkey] "p"
                   (fn [] (awful.spawn "rofi -show drun"))
                   {:description "launch apps" :group "launcher"})

;; FIXME: Broken
;; (define-global-key [modkey "Shift"] "p"
;;         (fn [] (rofi :items (client_names_rofi)
;;                      :on-select tag.add_client_to_tag_by_name
;;                      :title "New Tag"))
;;         {:description "select new tag for client" :group "launcher"})

(define-global-key [modkey] "o"
                   (fn [] (rofi {:items (tag.tag_names_rofi)
                                 :on-select tag.view_tag_by_name
                                 :title "Switch Tag"}))
                   {:description "switch tags" :group "launcher"})

(define-global-key [modkey "Shift"] "o"
                   (fn [] (rofi {:items (tag.tag_names_rofi)
                                 :on-select tag.send_client_to_tag_by_name
                                 :title "Switch Tag"}))
                   {:description "select new tag for client" :group "launcher"})

;; App Shortcuts
(define-global-key [modkey "Shift"] "Return"
                   (fn [] (awful.spawn config.terminal))
                   {:description "open a terminal" :group "launcher"})

(define-global-key [modkey "Shift"] "x"
                   (fn [] (awful.spawn "emacsclient -c"))
                   {:description "start emacsclient" :group "launcher"})

(define-global-key [modkey "Shift"] "v"
                   (fn [] (awful.spawn "emacsclient -c -e (vterm)"))
                   {:description "start emacsclient" :group "launcher"})

;; Navigate Tags
(define-global-key [modkey] "Left"
                   awful.tag.viewprev
                   {:description "view previous" :group "tag"})

(define-global-key [modkey] "Right"
                   awful.tag.viewnext
                   {:description "view next" :group "tag"})

(define-global-key [modkey] "Escape"
                   awful.tag.history.restore
                   {:description "go back" :group "tag"})

;; Navigate Windows
(define-global-key [modkey] "j"
                   (fn [] (awful.client.focus.byidx 1))
                   {:description "focus next by index" :group "client"})

(define-global-key [modkey] "k"
                   (fn [] (awful.client.focus.byidx -1))
                   {:description "focus previous by index" :group "client"})

(define-global-key [modkey] "u"
                   awful.client.urgent.jumpto
                   {:description "jump to urgent client" :group "client"})

;; Move Windows within Screen
(define-global-key [modkey "Shift"] "j"
                   (fn [] (awful.client.swap.byidx 1))
                   {:description "swap with next client by index" :group "client"})

(define-global-key [modkey "Shift"] "k"
                   (fn [] (awful.client.swap.byidx -1))
                   {:description "swap with previous client by index" :group "client"})

;; Navigate between Screens
(define-global-key [modkey] "e"
                   (fn [] (awful.screen.focus_relative 1))
                   {:description "focus the next screen" :group "screen"})

(define-global-key [modkey] "w"
                   (fn [] (awful.screen.focus_relative -1))
                   {:description "focus the previous screen" :group "screen"})

;; Move Windows between Screens
(define-global-key [modkey "Shift"] "e"
                   (fn []
                     (let [screen (awful.screen.focused)
                           selected_tag screen.selected_tag]
                       (awful.screen.focus_relative 1)
                       (let [next_screen (awful.screen.focused)
                             next_tag next_screen.selected_tag]
                         (when selected_tag
                           (sharedtags.movetag selected_tag next_screen)
                           (sharedtags.viewonly selected_tag next_screen))
                         (when next_tag
                           (sharedtags.movetag next_tag screen)
                           (sharedtags.viewonly next_tag screen))
                         (awful.screen.focus_relative -1))))
                   {:description "focus the next screen" :group "screen"})

(define-global-key [modkey "Shift"] "w"
                   (fn []
                     (let [screen (awful.screen.focused)
                           selected_tag screen.selected_tag]
                       (awful.screen.focus_relative -1)
                       (let [next_screen (awful.screen.focused)
                             next_tag next_screen.selected_tag]
                         (when selected_tag
                           (sharedtags.movetag selected_tag next_screen)
                           (sharedtags.viewonly selected_tag next_screen))
                         (when next_tag
                           (sharedtags.movetag next_tag screen)
                           (sharedtags.viewonly next_tag screen))
                         (awful.screen.focus_relative 1))))
                   {:description "focus the previous screen" :group "screen"})

;; Resize Layout
(define-global-key [modkey] "l"
                   (fn [] (awful.tag.incmwfact 0.05))
                   {:description "increase master width factor" :group "layout"})

(define-global-key [modkey] "h"
                   (fn [] (awful.tag.incmwfact -0.05))
                   {:description "decrease master width factor" :group "layout"})

;; Rearrange Layout
(define-global-key [modkey "Shift"] "h"
                   (fn [] (awful.tag.incnmaster  1 nil true))
                   {:description "increase the number of master clients" :group "layout"})

(define-global-key [modkey "Shift"] "l"
                   (fn [] (awful.tag.incnmaster -1 nil true))
                   {:description "decrease the number of master clients" :group "layout"})
;; awful.key({ modkey, "Control" }, "h",     function () awful.tag.incncol( 1, nil, true)    end,
;;    {description = "increase the number of columns", group = "layout"}),
;; awful.key({ modkey, "Control" }, "l",     function () awful.tag.incncol(-1, nil, true)    end,
;;    {description = "decrease the number of columns", group = "layout"}),

;; Switch Layouts
(define-global-key [modkey] "space"
                   (fn [] (awful.layout.inc 1))
                   {:description "select next layout" :group "layout"})

(define-global-key [modkey "Shift"] "space"
                   (fn [] (awful.layout.inc -1))
                   {:description "select previous layout" :group "layout"})

;; Restore Minimized
(define-global-key [modkey "Control"] "n"
                   (fn []
                     (let [c (awful.client.restore)]
                       ;; Focus restored client
                       (when c
                         (c:emit_signal
                          "request::activate"
                          "key.unminimize"
                          {:raise true}))))
                   {:description "restore minimized" :group "client"})

;; Prompt
;;   awful.key({ modkey },            "r",
;;      function () awful.screen.focused():emit_signal("ian::promptbox-run") end,
;;      {description = "run prompt", group = "launcher"}),
;;
;;   awful.key({ modkey }, "x",
;;      function () awful.screen.focused():emit_signal("ian::promptbox-execute") end,
;;      {description = "lua execute prompt", group = "awesome"}),


;; UI
(define-global-key [modkey] "b"
                   (fn []
                     (let [bar (. (awful.screen.focused) :mywibox)]
                       (set bar.visible (not bar.visible))))
                   {:description "toggle bar visibility" :group "ui"})

(define-global-key [modkey] "g"
                   (fn []
                     (let [screen (awful.screen.focused)
                           selected_tag screen.selected_tag]
                       (when selected_tag
                         (if (= selected_tag.gap 0)
                             (set selected_tag.gap beautiful.useless_gap)
                             (set selected_tag.gap 0)))))
                   {:description "toggle gaps" :group "ui"})

                                        ; Hardware
(define-global-key [] "XF86AudioRaiseVolume" volume.raise_volume)
(define-global-key [] "XF86AudioLowerVolume" volume.lower_volume)
(define-global-key [] "XF86AudioMute" volume.mute_volume) 

(define-global-key [modkey "Shift"] "b"
                   bluetooth.connect-goodphones
                   {:description "connect goodphones" :group "hardware"})

;; Numbered Tags
(for [i 1 9]
  ;; View tag only.
  (define-global-key [modkey] (.. "#" (+ i 9))
                     (fn []
                       (let [t (tag.get_tag i)]
                         (when t
                           (tag.view_only t))))
                     {:description (.. "view tag #" i) :group "tag"})

  ;; Toggle tag
  (define-global-key [modkey "Control"] (.. "#" (+ i 9))
                     (fn []
                       (let [t (tag.get_tag i)]
                         (when t
                           ;; TODO: This should toggle instead of just view
                           (tag.view_only t))))
                     {:description (.. "toggle tag #" i) :group "tag"})

  ;; Move client to tag.
  (define-global-key [modkey "Shift"] (.. "#" (+ i 9))
                     (fn []
                       (when client.focus
                         (let [t (tag.get_tag i)]
                           (when t
                             (client.focus:move_to_tag t)))))
                     {:description (.. "move focused client to tag #" i) :group "tag"})

  ;; Toggle tag on focused client.
  (define-global-key [modkey "Control" "Shift"] (.. "#" (+ i 9))
                     (fn []
                       (when client.focus
                         (let [t (tag.get_tag i)]
                           (when t
                             (client.focus:toggle_tag t)))))
                     {:description (.. "toggle focused client on tag #" i) :group "tag"}))
