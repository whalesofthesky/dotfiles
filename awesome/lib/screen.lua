local awful = require("awful")
local create_bar = require("lib.component.bar")
local laptop = require("lib.laptop")
local naughty = require("naughty")
local tag = require("lib.tag")
local volume = require("lib.volume")
local wallpaper = require("lib.wallpaper")

-- Re-set wallpaper when a screen's geometry changes (e.g. different resolution)
screen.connect_signal("property::geometry", wallpaper.set_wallpaper)

cleanup_map = {}

awful.screen.connect_for_each_screen(function(s)
      -- Wallpaper
      wallpaper.set_wallpaper(s)

      -- Each screen has its own tag table.
      if not tag.use_shared_tags then
         tag.create_default_tags(s)
      end

      laptop.get_is_laptop(function(is_laptop)
            res, err = pcall(function()
                  local cleanup = create_bar(s, is_laptop)
                  cleanup_map[s] = cleanup
            end)

            if not res then
               naughty.notify({ preset = naughty.config.presets.critical,
                                title = "Oops, an error happened!",
                                text = tostring(err) })
            end
      end)
end)

awful.screen.disconnect_for_each_screen(function(s)
      local cleanup = cleanup_map[s]
      if cleanup then
         cleanup()
      end
      table.remove(cleanup_map, s)
end)
