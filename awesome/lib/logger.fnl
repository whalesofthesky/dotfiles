(fn log-timestamp []
  (os.date "%Y/%m/%d %X" (os.time)))

(fn log [msg]
  (print (.. "[" (log-timestamp) "] " msg)))

(fn log-error [msg]
  (log msg))

(fn log-info [msg]
  (log msg))

(var logger {:info log-info
             :error log-error})

; Return
logger
