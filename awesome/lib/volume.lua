local awful = require("awful")

local volume = {}

volume.raise_volume = function()
   awful.spawn("pamixer -i 5")
end

volume.lower_volume = function()
   awful.spawn("pamixer -d 5")
end

volume.mute_volume = function()
   awful.spawn.easy_async(
      "pamixer --get-mute",
      function(stdout)
         if string.find(stdout, "true") == 1 then
            awful.spawn("pamixer -u")
         else
            awful.spawn("pamixer -m")
         end
      end
   )
end

volume.get_volume = function(callback)
   awful.spawn.easy_async(
      "pamixer --get-volume",
      callback
   )
end

volume.get_mute = function(callback)
   awful.spawn.easy_async(
      "pamixer --get-mute",
      function(stdout)
         callback(string.find(stdout, "true") == 1)
      end
   )
end

return volume
