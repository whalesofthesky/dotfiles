(local awful (require "awful"))

(fn rofi-index [config]
  ""
  (awful.spawn.easy_async_with_shell
   (.. "echo -e \""
       (if (= (type config.items) "string")
           config.items
           (table.concat config.items "\n"))
       "\" | rofi -dmenu -i -format i -p \""
       config.title
       "\"")
   (fn [res] (config.on-select (tonumber res)))))

rofi-index
