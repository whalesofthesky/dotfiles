(local awful (require "awful"))

(fn rofi [config]
  ""
  (awful.spawn.easy_async_with_shell
   (.. "echo -e \""
       (if (= (type config.items) "string")
           config.items
           (table.concat config.items "\n"))
       "\" | rofi -dmenu -i -p \""
       config.title
       "\"")
   config.on-select))

; Return
rofi
