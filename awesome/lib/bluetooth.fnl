(local awful (require :awful))
(local logger (require :lib.logger))

(fn connect-goodphones []
  (awful.spawn.with_line_callback
   "manager goodphones"
   {:stdout logger.info
    :stderr logger.error}))

(local bluetooth
       {:connect-goodphones connect-goodphones})

; Return
bluetooth
