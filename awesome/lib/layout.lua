local awful = require("awful")

awful.layout.layouts = {
    awful.layout.suit.tile.right,
    awful.layout.suit.max,
    awful.layout.suit.max.fullscreen,
}
