(local awful (require :awful))
(local beautiful (require :beautiful))
(local gears (require :gears))
(local wibox (require :wibox))

(local config (require :lib.var))
(local modkey config.modkey)

(local rofi (require :lib.rofi))
(local tag (require :lib.tag))

(local client-buttons {})
(fn define-client-button [mods key on-press data]
  "Define a global key binding"
  (gears.table.merge
   client-buttons
   (awful.button mods key on-press data)))

(local client-keys {})
(fn define-client-key [mods key on-press data]
  "Define a global key binding"
  (gears.table.merge
   client-keys
   (awful.key mods key on-press data)))

(define-client-key [modkey] "f"
                   (fn [c]
                     (set c.fullscreen (not c.fullscreen))
                     (c:raise))
                   {:description "toggle fullscreen" :group "client"})

(define-client-key [modkey "Shift"] "c"
                   (fn [c] (c:kill))
                   {:description "close" :group "client"})

(define-client-key [modkey] "t"
                   awful.client.floating.toggle
                   {:description "toggle floating" :group "client"})

(define-client-key [modkey] "Return"
                   (fn [c] (c:swap (awful.client.getmaster)))
                   {:description "move to master" :group "client"})

;; awful.key({ modkey,           }, "o",      function (c) c:move_to_screen()               end,
;;           {description = "move to screen", group = "client"}),

(define-client-key [modkey "Shift"] "t"
                   (fn [c] (set c.ontop (not c.ontop)))
                   {:description "toggle keep on top" :group "client"})

(define-client-key [modkey] "n"
                   (fn [c]
                     ;; The client currently has the input focus, so it cannot be
                     ;; minimized, since minimized clients can't have the focus.
                     (set c.minimized true))
                   {:description "minimize" :group "client"})

(define-client-key [modkey] "m"
                   (fn [c]
                     (set c.maximized (not c.maximized))
                     (c:raise))
                   {:description "(un)maximize" :group "client"})

(define-client-key [modkey "Control"] "m"
                   (fn [c]
                     (set c.maximized_vertical (not c.maximized_vertical))
                     (c:raise))
                     {:description "(un)maximize vertically" :group "client"})

(define-client-key [modkey "Shift"] "m"
                   (fn [c]
                     (c.maximized_horizontal (not c.maximized_horizontal))
                     (c:raise))
                   {:description "(un)maximize horizontally" :group "client"})

(define-client-key [modkey "Shift"] "s"
                   (fn [c]
                     (set c.ontop (not c.ontop))
                     (c:raise))
                   {:description "sticky" :group "client"})

(define-client-button [] 1
                      (fn [c] (c:emit_signal "request::activate"
                                             "mouse_click"
                                             {:raise true})))

(define-client-button [modkey] 1
                      (fn [c]
                        (c:emit_signal "request::activate"
                                       "mouse_click"
                                       {:raise true})
                        (awful.mouse.client.move c)))

(define-client-button [modkey] 3
                      (fn [c]
                        (c:emit_signal "request::activate"
                                       "mouse_click"
                                       {:raise true})
                        (awful.mouse.client.resize c)))

(local rule-map {})

(fn define-rule [name rule-def]
  ""
  (tset rule-map name rule-def)
  (set awful.rules.rules rule-map))

(local rules
       ;; All clients will match this rule.
       [{:rule {}
         :properties {:border_width beautiful.border_width
                      :border_color beautiful.border_normal
                      :focus awful.client.focus.filter
                      :raise true
                      :keys client-keys
                      :buttons client-buttons
                      :screen awful.screen.preferred
                      :placement (+ awful.placement.no_overlap awful.placement.no_offscreen)
                      :size_hints_honor false}}

        ;; Floating clients.
        {:rule_any {:instance ["DTA" ;; Firefox addon DownThemAll.
                                "copyq" ;; Includes session name in class.
                                "pinentry"]
                     :class ["Arandr"
                             "Blueman-manager"
                             "Gpick"
                             "Kruler"
                             "MessageWin" ;; kalarm.
                             "Sxiv"
                             "Tor Browser" ;; Needs a fixed window size to avoid fingerprinting by screen size.
                             "Wpa_gui"
                             "veromix"
                             "xtightvncviewer"]

                     ;; Note that the name property shown in xprop might be set slightly after creation of the client
                     ;; and the name shown there might not match defined rules here.
                     :name ["Event Tester"  ;; xev.
                            "(Emenu)"]
                     :role ["AlarmWindow" ;; Thunderbird's calendar.
                            "ConfigManager" ;; Thunderbird's about:config.
                            "pop-up"      ;; e.g. Google Chrome's (detached) Developer Tools.
                            ]}
          :callback (fn [c]
                      (when (not (string.find (string.lower c.name) "slack"))
                        (set c.floating true)))}

        ;; Add titlebars to normal clients and dialogs
        {:rule_any {:type ["dialog"]}
         :properties {:titlebars_enabled true}}

        {:rule_any {:class ["zoom"]}
         :properties {:tag "zoom"}}

        ;; Set Firefox to always map on the tag named "2" on screen 1.
        ;; { rule = { class = "Firefox" },
        ;;   properties = { screen = 1, tag = "2" } },
])

(set awful.rules.rules rules)

;; Only want borders if there's more than one window
(fn set-border-widths [screen]
  ""
  (when screen
    (var count 0)
    (each [i client (pairs screen.clients)]
      (set count (+ count 1))
    (each [i client (pairs screen.clients)]
      (if (= count 1)
          (set client.border_width 0)
          (set client.border_width beautiful.border_width))))))

;; {{{ Signals
;; Signal function to execute when a new client appears.
(client.connect_signal "manage" (fn [c]
                                  ;; Set the windows at the slave,
                                  ;; i.e. put it at the end of others instead of setting it master.
                                  (when (not awesome.startup) (awful.client.setslave c))

                                  (when (and awesome.startup
                                             (not c.size_hints.user_position)
                                             (not c.size_hints.program_position))
                                    ;; Prevent clients from being unreachable after screen count changes.
                                    (awful.placement.no_offscreen c))
                                  (set-border-widths c.screen)))

(client.connect_signal "unmanage" (fn [c]
                                    (set-border-widths c.screen)))

;; Add a titlebar if titlebars_enabled is set to true in the rules.
(client.connect_signal
 "request::titlebars"
 (fn [c]
   ;; buttons for the titlebar
   (let [buttons (gears.table.join
                  (awful.button {} 1 (fn []
                                       (c:emit_signal "request::activate"
                                                      "titlebar"
                                                      {:raise true})
                                       (awful.mouse.client.move c)))
                  (awful.button {} 3 (fn []
                                       (c:emit_signal "request::activate"
                                                      "titlebar"
                                                      {:raise true})
                                       (awful.mouse.client.resize c))))]
     (: (awful.titlebar c)
        :setup
        {1 { ;; Left
            1 (awful.titlebar.widget.iconwidget c)
             :buttons buttons
             :layout wibox.layout.fixed.horizontal}
         2 { ;; Middle
            1 { ;; Title
               :align  "center"
                :widget (awful.titlebar.widget.titlewidget c)}
             :buttons buttons
             :layout wibox.layout.flex.horizontal}
         3 {
            1 (awful.titlebar.widget.floatingbutton c)
            2 (awful.titlebar.widget.maximizedbutton c)
            3 (awful.titlebar.widget.stickybutton c)
            4 (awful.titlebar.widget.ontopbutton c)
            5 (awful.titlebar.widget.closebutton c)
            :layout (wibox.layout.fixed.horizontal)}
         :layout wibox.layout.align.horizontal}))))

;; Enable sloppy focus, so that focus follows mouse.
(client.connect_signal "mouse::enter" (fn [c]
                         (c:emit_signal "request::activate" "mouse_enter" {:raise false})))

(client.connect_signal
   "request::activate"
   (fn [c]
      (when (and c c.first_tag (not (c:isvisible)))
         (tag.view_only c.first_tag))))

(client.connect_signal "focus" (fn [c] (set c.border_color beautiful.border_focus)))
(client.connect_signal "unfocus" (fn [c] (set c.border_color beautiful.border_normal)))
