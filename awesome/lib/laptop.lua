local awful = require("awful")

local laptop = {}

laptop.get_is_laptop = function(fn)
   awful.spawn.easy_async(
      "hostname",
      function(stdout)
         local is_laptop = false
         local laptops = {"grubbins"}

         for i,laptop in pairs(laptops) do
            if stdout == laptop .. "\n" then
               is_laptop = true
               break
            end
         end

         fn(is_laptop)
      end
   )
end

laptop.get_battery_state = function(callback)
   awful.spawn.easy_async(
      "acpi -b",
      function(out)
         charge, value = string.match(out, "([A-z][%sA-z]+), (%d+).")
         callback(charge, value)
      end
   )
end

return laptop
