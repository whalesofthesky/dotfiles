local beautiful = require("beautiful")
local create_bar_box = require("lib.component.bar_box")
local naughty = require("naughty")
local volume = require("lib.volume")
local wibox = require("wibox")
local logger = require("lib.logger")

return function(widget_bg)
   local volumebox = wibox.widget {
      widget = wibox.widget.textbox,
   }

   local volume_wrapper, text_box = create_bar_box(
      { volumebox, "%" },
      widget_bg
   )

   return volume_wrapper, function ()
      logger.info("volume_box: execute timer")

      volume.get_volume(function(value)
            volumebox:set_text("Volume: " .. value)
      end)

      volume.get_mute(function(is_mute)
            if is_mute then
               text_box.fg = "#ff0000"
            else
               text_box.fg = beautiful.fg_normal
            end
      end)
   end
end
