local awful = require("awful")
local beautiful = require("beautiful")
local create_battery_box = require("lib.component.battery_box")
local create_tasklist_buttons = require("lib.component.tasklist_buttons")
local create_volume_box = require("lib.component.volume_box")
local logger = require("lib.logger")
local gears = require("gears")
local naughty = require("naughty")
local wibox = require("wibox")

return function(s, is_laptop)
   logger.info("bar: create new bar")
   local clock = wibox.widget.textclock()

   -- Create a promptbox for each screen
   local promptbox = awful.widget.prompt()

   s:connect_signal("ian::promptbox-run", function() promptbox:run() end)
   s:connect_signal(
      "ian::promptbox-execute", 
      function ()
         awful.prompt.run {
            prompt       = "Run Lua code: ",
            textbox      = promptbox.widget,
            exe_callback = awful.util.eval,
            history_path = awful.util.get_cache_dir() .. "/history_eval"
         }
   end)

   -- Create an imagebox widget which will contain an icon indicating which layout we're using.
   -- We need one layoutbox per screen.
   layoutbox = awful.widget.layoutbox(s)
   layoutbox:buttons(gears.table.join(
                            awful.button({ }, 1, function () awful.layout.inc( 1) end),
                            awful.button({ }, 3, function () awful.layout.inc(-1) end),
                            awful.button({ }, 4, function () awful.layout.inc( 1) end),
                            awful.button({ }, 5, function () awful.layout.inc(-1) end)))

   -- Create a tasklist widget
   tasklist = awful.widget.tasklist {
      screen  = s,
      filter  = awful.widget.tasklist.filter.currenttags,
      buttons = create_tasklist_buttons()
   }

   -- Create the wibox
   s.mywibox = awful.wibar({
         position = "top",
         screen = s,
         widget = wibox.container.background,
         bg="#00000000"
   })

   local widget_bg = beautiful.bg_normal .. "c0"

   local volume_wrapper, volume_timer = create_volume_box(widget_bg)

   if is_laptop then
      local battery_wrapper, battery_timer = create_battery_box(widget_bg)
   end

   local timer = gears.timer {
      timeout   = 1,
      call_now  = true,
      autostart = true,
      callback  = function()
         logger.info("bar: execute timers")
         volume_timer()

         if is_laptop then
            battery_timer()
         end
      end
   }

   -- Add widgets to the wibox

   local components = {
      volume_wrapper,
      {
         clock,
         widget = wibox.container.background,
         bg=widget_bg,
      },
      layout = wibox.layout.fixed.horizontal,
   }

   if is_laptop then
      table.insert(components, 1, battery_wrapper)
   end

   local margin_wrapped = wibox.widget {
      components,
      color = beautiful.bg_focus,
      spacing = 5,
      left = 1,
      right = 1,
      top = 1,
      bottom = 1,
      widget = wibox.container.margin,
   }

   s.mywibox:setup {
      layout = wibox.layout.align.horizontal,
      { -- Left widgets
         layout = wibox.layout.fixed.horizontal,
         promptbox,
      },
      tasklist, -- Middle widget
      { -- Right widgets
         margin_wrapped,
         {
            layoutbox,
            widget = wibox.container.background,
            bg = beautiful.bg_normal,
         },
         layout = wibox.layout.fixed.horizontal
      },
   }

   return function()
      logger.info("bar: cleanup timer")
      timer.stop()
   end
end
