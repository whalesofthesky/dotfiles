local beautiful = require("beautiful")
local laptop = require("lib.laptop")
local wibox = require("wibox")

return function ()
   local batterybox = wibox.widget {
      widget = wibox.widget.textbox,
   }

   local battery_percent =  wibox.widget {
      widget = wibox.widget.textbox,
      text = "%",
   }

   local battery = wibox.widget {
      {
         batterybox,
         battery_percent,
         layout = wibox.layout.fixed.horizontal
      },
      widget = wibox.container.background
   }

   local battery_widget_bg = beautiful.bg_normal .. "c0"

   local battery_wrapper = wibox.widget {
      {
         {
            battery,
            widget = wibox.container.margin,
            left = 15,
            right = 15,
         },
         widget = wibox.container.background,
         bg = battery_widget_bg,
      },
      color = beautiful.bg_focus,
      left = 1,
      right = 1,
      widget = wibox.container.margin
   }

   return battery_wrapper, function()
      laptop.get_battery_state(function(charge, value)
            if value then
               batterybox:set_text("Battery: " .. value)

               if charge == "Charging" then
                  battery.fg = "#00ff00"
               else
                  battery.fg = beautiful.fg_normal
               end
            end
      end)
   end
end
