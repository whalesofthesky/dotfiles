local beautiful = require("beautiful")
local wibox = require("wibox")

local function create_bar_box(children, widget_bg)
   local widget_boxes = {}

   for i, child in pairs(children) do
      if type(child) == "string" then
         widget_boxes[i] = wibox.widget {
            text = child,
            widget = wibox.widget.textbox,
         }
      else
         widget_boxes[i] = child
      end
   end

   widget_boxes.layout = wibox.layout.fixed.horizontal 

   local combined_text_box = wibox.widget {
      widget_boxes,
      widget = wibox.container.background
   }

   local margin_box = wibox.widget {
      {
         {
            combined_text_box,
            widget = wibox.container.margin,
            left = 15,
            right = 15,
         },
         widget = wibox.container.background,
         bg = widget_bg,
      },
      color = beautiful.bg_focus,
      left = 1,
      right = 1,
      widget = wibox.container.margin
   }

   return margin_box, combined_text_box
end

return create_bar_box
