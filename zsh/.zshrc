#! /usr/bin/env zsh

alias mgr="manager"
export XDG_CONFIG_HOME=/home/ian/.config
export IAN_BACKGROUND_PATH=/mnt/home/ian/Pictures/system/wallpaper/shovel-knight_camp.png

# https://github.com/ohmyzsh/ohmyzsh/issues/6835
ZSH_DISABLE_COMPFIX=true
# Path to your oh-my-zsh installation.
export ZSH="$HOME/.oh-my-zsh"
# See https://github.com/ohmyzsh/ohmyzsh/wiki/Themes
ZSH_THEME="robbyrussell"
plugins=(git)
source $ZSH/oh-my-zsh.sh

# TODO: Do I need all of these?
[ -f /usr/share/fzf/key-bindings.zsh ] && source /usr/share/fzf/key-bindings.zsh
[ -f /usr/share/fzf/completion.zsh ] && source /usr/share/fzf/completion.zsh
[ -f ~/.fzf.zsh ] && source ~/.fzf.zsh

# https://github.com/akermu/emacs-libvterm#shell-side-configuration
vterm_printf(){
    if [ -n "$TMUX" ] && ([ "${TERM%%-*}" = "tmux" ] || [ "${TERM%%-*}" = "screen" ] ); then
        # Tell tmux to pass the escape sequences through
        printf "\ePtmux;\e\e]%s\007\e\\" "$1"
    elif [ "${TERM%%-*}" = "screen" ]; then
        # GNU screen (screen, screen-256color, screen-256color-bce)
        printf "\eP\e]%s\007\e\\" "$1"
    else
        printf "\e]%s\e\\" "$1"
    fi
}

vterm_prompt_end() {
    vterm_printf "51;A$(whoami)@$(hostname):$(pwd)";
}

setopt PROMPT_SUBST
PROMPT=$PROMPT'%{$(vterm_prompt_end)%}'

[ -f "/home/ian/.ghcup/env" ] && source "/home/ian/.ghcup/env" # ghcup-env

export NVM_DIR="$HOME/.nvm"
[ -s "$NVM_DIR/nvm.sh" ] && \. "$NVM_DIR/nvm.sh"  # This loads nvm
[ -s "$NVM_DIR/bash_completion" ] && \. "$NVM_DIR/bash_completion"  # This loads nvm bash_completion
[ -s ~/.luaver/luaver ] && . ~/.luaver/luaver

HOME_FILE="$IAN_SYSTEM_CONFIG/zsh/.zshrc"

if [ -f "$HOME_FILE" ]; then
    source $HOME_FILE
fi

LOCAL_FILE=~/.zshrc-local

if [ -f "$LOCAL_FILE" ]; then
    source $LOCAL_FILE
fi

#THIS MUST BE AT THE END OF THE FILE FOR SDKMAN TO WORK!!!
export SDKMAN_DIR="/home/ian/.sdkman"
[[ -s "/home/ian/.sdkman/bin/sdkman-init.sh" ]] && source "/home/ian/.sdkman/bin/sdkman-init.sh"

export PATH=$PATH:~/bin:~/.roswell/bin:~/Code/oss/android/flutter/bin
export CHROME_EXECUTABLE=google-chrome-stable
export GIT_DISCOVERY_ACROSS_FILESYSTEM=1
export VOLTA_HOME="$HOME/.volta"
export PATH="$VOLTA_HOME/bin:$PATH"
