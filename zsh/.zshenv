export IAN_CODE="$HOME/Code"
export IAN_CODE_OSS="$IAN_CODE/oss"
export IAN_CODE_SYSTEM="$IAN_CODE/system"
export IAN_LOCAL="$IAN_CODE_SYSTEM/local"
export IAN_MANAGER="$IAN_CODE_SYSTEM/manager"

export IAN_PROGRAMS="$HOME/Programs/linux"
export IAN_AUR="$IAN_PROGRAMS/aur-packages"

export IAN_DOCUMENTS="$HOME/Documents"
export IAN_CONTENT="$IAN_DOCUMENTS/content"
export IAN_ORG="$IAN_DOCUMENTS/org"
export IAN_SYSTEM="$IAN_DOCUMENTS/system"
export IAN_SYSTEM_CONFIG="$IAN_SYSTEM/config"
export MANAGER_CONFIG_HOME="$IAN_SYSTEM_CONFIG/manager"

export PATH=$HOME/Code/project/active/manager/bin:$HOME/Code/system/dotfiles/bin:$PATH

LOCAL_FILE="$IAN_LOCAL/zsh/.zshenv"

if [ -f "$LOCAL_FILE" ]; then
    source $LOCAL_FILE
fi

HOME_FILE="$IAN_SYSTEM_CONFIG/zsh/.zshenv"

if [ -f "$HOME_FILE" ]; then
    source $HOME_FILE
fi
